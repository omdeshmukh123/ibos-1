package com.intellect.ibos.signverify;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import com.intellect.ibos.Utils.IBOSConstants;

public class Utility {

    private static final String STORE_TYPE = "PKCS12";
    private static final char[] PASSWORD = "1234".toCharArray();
    
    // Intellect Private Key
    private static final String SENDER_KEYSTORE = "/usr1/SIR21466/Store/MyKeys/privatekey.p12";
    //private static final String SENDER_KEYSTORE = "MyKeys/privatekey.p12";
    private static final String SENDER_ALIAS = "1";

    public static final String SIGNING_ALGORITHM = "SHA256withRSA";
    public static final String PAYLOAD_FILE = "D:\\SSL_JWE_AUTH\\payload\\payload.json";

    public static PrivateKey getPrivateKey() throws Exception {
    	try {
        KeyStore keyStore = KeyStore.getInstance(STORE_TYPE);
        keyStore.load(new FileInputStream(SENDER_KEYSTORE), PASSWORD);
        return (PrivateKey) keyStore.getKey(SENDER_ALIAS, PASSWORD);
    	} catch(Exception e) {
			throw new Exception("Invalid File Path");// checked exception
    	}
    }
    
    public static PublicKey getPublicKey() throws Exception {
	try {
    	File f = new File(IBOSConstants.RECEIVER_KEYSTORE);
		FileInputStream fis = new FileInputStream(f);
		CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
		X509Certificate certificate = (X509Certificate) certFactory.generateCertificate(fis);
		fis.close();
		PublicKey pk = certificate.getPublicKey();       
        return pk;
    } catch(Exception e) {
		throw new Exception("Invalid File Path");// checked exception
	}
    }
}