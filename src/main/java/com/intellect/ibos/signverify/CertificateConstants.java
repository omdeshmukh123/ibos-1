package com.intellect.ibos.signverify;

public final class CertificateConstants {
    // separators:
    public static final String EQUALS_SIGN_SEPARATOR = "=";
    public static final String COMMA_SEPARATOR = ",";
    public static final String SPACE_SEPARATOR = " ";
    public static final String HEXADECIMAL_SPACE_SEPARATOR = "%20";

    // certificates:
    public static final String CERTIFICATE_SERIAL_NUMBER_ATTRIBUTE = "SN";
    public static final String CERTIFICATION_AUTHORITY_ATTRIBUTE = "CA";

    private CertificateConstants() {
    }
}