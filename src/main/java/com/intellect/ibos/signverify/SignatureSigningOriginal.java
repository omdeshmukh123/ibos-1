package com.intellect.ibos.signverify;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.tomitribe.auth.signatures.Signature;
import org.tomitribe.auth.signatures.Signer;

public class SignatureSigningOriginal {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
    //private static final String RECEIVER_KEYSTORE = "MyKeys/QSealC-NordeaDevPortal.cer";
    private static final String RECEIVER_KEYSTORE = "MyKeys/publickey.cer";

    public void sign() throws Exception {
    	
        // Request Body to be sent to AHB
        String httpPayload = new String(Files.readAllBytes(Paths.get(Utility.PAYLOAD_FILE)), "UTF-8");  
        System.out.println("\n--httpPayload--"+httpPayload);
        
		BufferedReader reader = new BufferedReader(new FileReader(RECEIVER_KEYSTORE));
		String cert = reader.lines().collect( Collectors.joining( System.lineSeparator() ) );
	    System.out.println("digest1"+generateDigest(httpPayload)+"\n------------Cert------------\n"+formatCertificate(cert));

        String digests ="SHA-256=" + new String(Base64.getEncoder().encode(DigestUtils.sha256(Files.readAllBytes(Paths.get(Utility.PAYLOAD_FILE)))));
        System.out.println("++++++++digest2+++++++++++"+digests);
		
        // Add the self-signed TPP Public Key
        Map<String, String> requestHeaders = new LinkedHashMap<>();
        requestHeaders.put("x-request-id", "56f8baa3-058b-4550-8587-912a35e0c08d");  
		requestHeaders.put("digest", generateDigest(httpPayload));
        requestHeaders.put("signature", generateSignature(requestHeaders));
      }

    private String formatCertificate(String certificate) {
    return certificate.replaceAll("\n", "")
    .replaceAll("\r", "")
    .replace("-----BEGIN CERTIFICATE-----", "")
    .replace("-----END CERTIFICATE-----", "");
    }

    private String generateDigest(String payload) {
    try {
    byte[] digest = MessageDigest.getInstance("SHA-256").digest(payload.getBytes(StandardCharsets.UTF_8));
    return "SHA-256=" + new String(Base64.getEncoder().encode(digest));
    } catch (NoSuchAlgorithmException e) {
    throw new IllegalArgumentException("Wrong algorithm", e);
    }
    }

    private String generateSignature(Map<String, String> requestHeaders) throws Exception {
    	SignatureVerifying ver = new SignatureVerifying();
     
    	// Obtain the TPP Signature Serial number and other details
    	File f = new File(RECEIVER_KEYSTORE);
		FileInputStream fis = new FileInputStream(f);
		CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
		X509Certificate certificate = (X509Certificate) certFactory.generateCertificate(fis);
		// Obtain the TPP Signature Serial Number etc
		String keyId = ver.getKeyIdFromCertificate(certificate);
		//String.format("SN=%s,CA=%s", certificate.getSerialNumber(), certificate.getIssuerDN());
		fis.close();
		
		BufferedReader reader = new BufferedReader(new FileReader(RECEIVER_KEYSTORE));
		String cert = reader.lines().collect( Collectors.joining( System.lineSeparator() ) );
		
    String algorithm = "rsa-sha256";
    Signature signature = new Signature(keyId, algorithm, null, requestHeaders.keySet().toArray(new String[0]));

    PrivateKey privateKey = getPrivateKey(); // IDAL Private Key
    Signer signer = new Signer(privateKey, signature);
    
    String method = "method";
    String uri = "uri";
    Signature signed = signer.sign(method, uri, requestHeaders);
    System.out.println(keyId+"\n********signed*******\n"+signed);
    
	System.out.println("Signature Verified:-["+ver.verify(signed.toString(), formatCertificate(cert), requestHeaders, method, uri)+"]\n");

    return signed.toString();

    }

    public static PrivateKey getPrivateKey() throws Exception {
    	try {
        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(new FileInputStream("MyKeys/privatekey.p12"), "1234".toCharArray());
        return (PrivateKey) keyStore.getKey("1", "1234".toCharArray());
    	} catch(Exception e) {
			throw new Exception("Invalid File Path");// checked exception
    	}
    }
   
    public static void main(String[] args) throws Exception {
              
    	SignatureSigningOriginal obj = new SignatureSigningOriginal();
    	
    	obj.sign();
        System.out.println("iiiiiiiiiiiiiiiiiiiii\n\n");        

    	SignatureVerifying ver = new SignatureVerifying();

		String cert = "MIIFrjCCA5agAwIBAgIEX5bWSTANBgkqhkiG9w0BAQsFADBgMQswCQYDVQQGEwJIUjEkMCIGA1UECgwbUHJpdnJlZG5hIEJhbmthIFphZ3JlYiBkLmQuMQ8wDQYDVQQLDAZQQlogSVQxGjAYBgNVBAMMEVBCWiBJQk9TIGlHVEIgUE9DMB4XDTIwMTAyNjEzNTkzN1oXDTIxMTAyNjEzNTkzN1owYDELMAkGA1UEBhMCSFIxJDAiBgNVBAoMG1ByaXZyZWRuYSBCYW5rYSBaYWdyZWIgZC5kLjEPMA0GA1UECwwGUEJaIElUMRowGAYDVQQDDBFQQlogSUJPUyBpR1RCIFBPQzCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAKeTN7mHCw4xwM/m4v2K62FRJCvSQn8etkBgPckvNF3eLCY3JrxU5hBBQSmk78zhWhNTA2DmP/qcBvh4j6dFsxRP7A7ZKf6tclSy//t4pWCEBNrjD9oqOcG6LfSs97q/qabeJ0DwHXaAp5AE9x4/wMub4o++F/tY65PTSDHL6dgJqv5NNkPtP1MI6R3AxBFJhKz7cYrtWA6U6nGIaoM5eyPVi5ZqQ73oR34AXYdGJiwyy5E9sBKeAeMGD2Rqjgi0wfb0+/vwSuoFssgQiwbHd17PN3xLUZUS2rv3C9bxbBuFgGawg83xwEpeEmGpLAbxKU3KvJV3MKZbIDJOqEhkk0o+fJ6mHYM1ndP2zTyRub1eVPiRYLY1q6qYnLg3hZDB+JsfMHmOG50BZBcGIUVHSFCvGT0nR+x1AE0FZVKN21IYoZxsPGeJaF7mUqhbRIwpO7GaXGHyD+qB2Rs3KH4nTmycM3jPCSpTTg+yhB1frExVLv1qAeABs79euACHSSsV4o+qpvFacCR8lQnVvyvq58kflLjP2HapwQpWmbqAGpVJBOclursGhXBspNncWKzYyvquyqZvmCrd6QQTamJsUhsK784XZxbeOws14Ak3Zgz0WB/c2KnTTN8hC6/NbyvThz1jxw6NSfSex9gYxtpqkvqqXnEYaaMmgh7QUpmbiDePAgMBAAGjcDBuMB8GA1UdIwQYMBaAFJGLp+uEgAntJo6trlmQLKTjNY+cMB0GA1UdDgQWBBSRi6frhIAJ7SaOra5ZkCyk4zWPnDALBgNVHQ8EBAMCBaAwHwYDVR0lBBgwFgYIKwYBBQUHAwIGCisGAQQBgjcKAwwwDQYJKoZIhvcNAQELBQADggIBAHczgL4xN1PGmCI+DYp+MPfDiX2axAsPlFpcp2ekWmbzZYGCrA2eyqzMXcV3KgzHfCK8k0IA38hQEhguSZdSA8RJtN9O3+N1rEUoEfASAOXa4+ovk9nh4aIgdmqp1c2HVVcbAIE3Zs6VV4p7GQyIez9xI7QYhN7z5OGltMcOqO0USCfypAD6gNuiNFtBQkEV62Lcz0byQVysJcOEtNL9YjZa32/7Ccgg1MTHt8wGmLFld4kqPc5BAhSnuAeyp927Qo+rKe9n3GKbHpQp688+OhjbAD9NOCWMdV38AUbEbfYvi4VdDEYxAeRiX0dmkcrSS8X3Rn281EN9nDCSZdBR1HVX7JhRKfvmPcgSUgoPUAtM8g4rQSdLhWV5JrKZVLwJMABo+knCqWjMXQIlOK1LOY2//VKyGEHEYpKXMsCCPj2cTLxkQfu3dUb0gwlNS5MHmk4hWw0V9fzKbd0bWAbsAlGpNz/HKcrhyd9SJhXXiftdODvTu4yUIi6cyurVfrGFyDBAoIGEfflg04+lv2frVOaU/FomXCeP+t1QLpkMRzO/NBC0zuXdPhH3dALZO4YpiFRXaEIZ6mNTMW+8PFDOEIiWmYJ6UXIQ7OAMaIRYdyXL1btZGQazavPj8awUKo+HiCl6sTvlYyRweJKJoW2jeHRDxeM2hUjojaIQRWDhiwtw";
	    
        Map<String, String> requestHeaders = new LinkedHashMap<>();
        requestHeaders.put("x-request-id", "56f8baa3-058b-4550-8587-912a35e0c08d");  
		requestHeaders.put("digest", "SHA-256=E1VwB2o31JWaMGfHk5rTbKb/8UrT3Hj4hr0hLvNpFH4=");
	    //requestHeaders.put("tpp-signature-certificate", obj.formatCertificate(cert));
        requestHeaders.put("signature", "keyId=\"SN=5F96D649,CA=CN=PBZ IBOS iGTB POC,OU=PBZ IT,O=Privredna Banka Zagreb d.d.,C=HR\",algorithm=\"rsa-sha256\",headers=\"digest x-request-id\",signature=\"VDwpQfnekefmBrVubZJenEgONPKjg59iHcanJnZW6NIoYR56ngUvFI7XKtcd8Ht/8ptEexXNzHsk2HbK+XItPYzLhvuTCZv7isNVWRVwz7WEnqBCesqD/BNKoLRVs/Z10hEuJDEy5UtwyeKhGTaM2gxobxGDWlAPUJPOpbRfxNaAGSmjiKzFMMu5RaSkXmp20fF8ZZONCyT0GhpY5KBtKyRnRKy1SXo9mSNifzCUCCS1b3/qL9ffkvdhLBwjMl7+8U+uv2inAzG9uR14TL+dCkR+wdgBi93FKIqbCHnUjBipp/ncHylJPzYlgnvAw3kmeYrMkpverpb5CsOq5w98+zoX3v+ZqCxYimaHPu0FnKJk+rJAxWmV82brPuzUshr5o1WFydpAjZWSVjoEwl5Qr//wnEDTpxKIY7qeUo1ny4uv6D/shTlIj1URtG3UxZDChfEldxRyXpymjf/5w9CBHFSrEpr0EjR5hEqrqoYAHZRvK47LPUcZW/Sg0YDsc95SOlpaNLW/BF1UB7gG8JVAYFSCQmJY+FfMskN5QgpPI7xHedi3vLkrZOE2PYM7Whumoi0sNM8j8l1VA4fCPHQ0EHtb2afuRNmnokJQYFP5Ea/z1HOypcv7wZW3p1r4Oa2oskt16xXZU58s8v5TTK8j+V89zlki5eWpBHZGepO8zCs=\"");
        
    	System.out.println("Signature Verified:-["+ver.verify(requestHeaders.get("signature").replaceAll("\r", ""), 
    			obj.formatCertificate(cert), requestHeaders, "method", "uri")+"]\n");

        System.out.println("cccccccccccccccccccccccccccccccccccccccccccccccccccc");        
    }
}