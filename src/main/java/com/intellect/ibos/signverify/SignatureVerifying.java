package com.intellect.ibos.signverify;

import java.security.cert.X509Certificate;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.tomitribe.auth.signatures.Signature;
import org.tomitribe.auth.signatures.Verifier;

import com.nimbusds.jose.util.X509CertUtils;

public class SignatureVerifying {

	// Signature, TPP Certificate, Header, method, url
	public boolean verify(String signature, String tppEncodedCert, Map<String, String> headers, String method,
			String url) {
		X509Certificate certificate = X509CertUtils.parse(CertificateUtils.normalizeCertificate(tppEncodedCert));

		if (certificate == null) {
			System.out.println("TPP Certificate couldn't be parsed!");
			return false;
		}

		Signature signatureData = Signature.fromString(signature);

		System.out.println(
				"\n--------HB signatureData -------:-\n" + signatureData);

		if (!isKeyIdValid(certificate, signatureData.getKeyId().replaceAll("\\r", ""))) {
			System.out.println("\n----------Key ID is invalid!----------\n");
			return false;
		}

		Map<String, String> headersMap = RequestHeaders.fromMap(headers).toMap();
		System.out.println("\n----------headersMap----------\n" + headersMap.toString());
		try {
			Verifier verifier = new Verifier(certificate.getPublicKey(), signatureData);
			return verifier.verify(method, url, headersMap);

		} catch (Exception e) {
			System.out.println("Signature verification has an error: {}" + e.getMessage());
			return false;
		}
	}

	private boolean isKeyIdValid(X509Certificate certificate, String keyId) {
		System.out.println("HB keyId:\n"+keyId+
				"_\n" + getKeyIdFromCertificate(certificate)+"_");
		/*
		int counter=0;
		
		char[] first  = keyId.toCharArray();
		char[] second = getKeyIdFromCertificate(certificate).toCharArray();

		int minLength = Math.min(first.length, second.length);

		for(int i = 0; i < minLength; i++)
		{

		        if (first[i] != second[i])
		        {
		        	System.out.println(first[i]+"***********"+second[i]);
		            counter++;    
		        } else {
		        	System.out.println(first[i]+"........."+second[i]);
		        }
		}
		*/
		return StringUtils.equalsIgnoreCase(keyId, getKeyIdFromCertificate(certificate).replaceAll("\\r", ""));
	}

	String getKeyIdFromCertificate(X509Certificate certificate) {
		return CertificateConstants.CERTIFICATE_SERIAL_NUMBER_ATTRIBUTE + CertificateConstants.EQUALS_SIGN_SEPARATOR
				+ certificate.getSerialNumber().toString(16) // toString(16) is used to provide hexadecimal coding as
																// mentioned in specification
				+ CertificateConstants.COMMA_SEPARATOR + CertificateConstants.CERTIFICATION_AUTHORITY_ATTRIBUTE
				+ CertificateConstants.EQUALS_SIGN_SEPARATOR + certificate.getIssuerX500Principal().getName();
				//.replace(CertificateConstants.SPACE_SEPARATOR, CertificateConstants.HEXADECIMAL_SPACE_SEPARATOR);
	}
}