package com.intellect.ibos.signverify;

import com.nimbusds.jose.util.X509CertUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.security.cert.X509Certificate;
import java.util.Arrays;

@Slf4j
public class CertificateUtils {
    private static final int CERTIFICATE_PART_DATA_SIZE = 64;
    private static final String BEGIN_CERTIFICATE = "-----BEGIN CERTIFICATE-----\n";
    private static final String END_CERTIFICATE = "-----END CERTIFICATE-----";

    private CertificateUtils() {
    }

    public static X509Certificate[] getCertificates(String folderName, String... fileNames) {
		return Arrays.stream(fileNames)
                .map(fileName -> getCertificate(folderName + "/" + fileName))
                .toArray(X509Certificate[]::new);
	}

	private static X509Certificate getCertificate(String filePath) {

		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		try {
			byte[] bytes = IOUtils.toByteArray(filePath);
            return X509CertUtils.parse(bytes);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public static String getCertificateByName(String filename) {

		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		try {
			byte[] bytes = IOUtils.toByteArray("certificates/" + filename);
			X509Certificate cert = X509CertUtils.parse(bytes);
            return X509CertUtils.toPEMString(cert);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

    /**
     * Normalizes certificate: removes excess blanks and wraps by beginning and end tags.
     *
     * @param certificate certificate text
     * @return normalized certificate
     * <p>
     * -----BEGIN CERTIFICATE-----
     * (certificate)
     * -----END CERTIFICATE-----
     */
    public static String normalizeCertificate(String certificate) {
        if (certificate == null) {
            return null;
        }
        String certificateData = getCertificateData(certificate);
        System.out.println("certificateData:"+certificateData);
        return BEGIN_CERTIFICATE +
                   certificateData.replaceAll(".{" + CERTIFICATE_PART_DATA_SIZE + "}", "$0" + StringUtils.LF) +
                   END_CERTIFICATE;
    }

    private static String getCertificateData(String certificate) {
        return certificate.replace(" ", "")
                   .replace("\n", "")
                   .replace("\r", "")
                   .replace("-----BEGINCERTIFICATE-----", "")
                   .replace("-----ENDCERTIFICATE-----", "");
    }
}