package com.intellect.ibos;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.intellect.ibos.Utils.MTXXXDao;
import com.intellect.ibos.Utils.SFTPConnectionUtilForPullingFiles;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;
import com.prowidesoftware.swift.model.mt.mt9xx.MT940;
import com.prowidesoftware.swift.model.mt.mt9xx.MT942;

@Component
public class MTXXXReceiverScheduler {

	private static final Logger log = LoggerFactory.getLogger(MTXXXReceiverScheduler.class);
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Value("${SANTANDER_TO_IBOS_MT940_INCOMING}")
	private String ibos_to_Santander_Incoming_path_MT940;

	@Value("${SANTANDER_TO_IBOS_MT940_FAILED}")
	private String ibos_to_Santander_Failed_path_MT940;

	@Value("${SANTANDER_TO_IBOS_MT940_BACKUP}")
	private String ibos_to_Santander_Backup_path_MT940;
	
	@Value("${SANTANDER_TO_IBOS_MT942_INCOMING}")
	private String ibos_to_Santander_Incoming_path_MT942;

	@Value("${SANTANDER_TO_IBOS_MT942_FAILED}")
	private String ibos_to_Santander_Failed_path_MT942;

	@Value("${SANTANDER_TO_IBOS_MT942_BACKUP}")
	private String ibos_to_Santander_Backup_path_MT942;
	
	
	@Value("${DB_url}")
	String url ;
	
	@Value("${DB_username}")
	String userName ;
	
	@Value("${DB_password}")
	String password ;

	//@Scheduled(fixedRate = 10000)
	//@Scheduled(cron = "10 0 9-16 * * *")
	//@Scheduled(cron = "0/60 * 00-17 ? * *")
	@Scheduled(cron = "0/60 * 9-17 ? * *")
	public void startProcessing() {
		// log.info("The time is now {}", dateFormat.format(new Date()));
		System.out.println("MTXXX Receiver SchedulerThe time is now {}" + dateFormat.format(new Date()));
		SFTPConnectionUtilForPullingFiles lConnectionUtil = new SFTPConnectionUtilForPullingFiles();
		ChannelSftp lsftp = null;
		DBConnectionUtil dbUtil = new DBConnectionUtil();
		Connection conn = null;
		
		try {
			lsftp = lConnectionUtil.getSFTPConnectionforPullingFiles();
			/*
			 * if(lsftp ==null || lsftp.isClosed() || !lsftp.isConnected()) {
			 * System.out.println("Connection is not established sftp so returning ");
			 * return; }
			 */
			//System.out.println(" Entering start processing for receiving "+url);
			conn = dbUtil.mGetConnection(url, userName, password);
			
			if(conn==null ||conn.isClosed() ) {
				System.out.println("DB connection is not established DB so returning ");
				return;
			}
			readMT940FromAHBSantander(ibos_to_Santander_Incoming_path_MT940 ,lsftp,conn);
			readMT942FromAHBSantander(ibos_to_Santander_Incoming_path_MT942 ,lsftp,conn);

		} catch (IOException | SftpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
//			if( lsftp != null) {
			lConnectionUtil.closeConnection();
//			}
			dbUtil.mCloseResources(null, conn);
		}
	}

	/*
	 * private void copyMessageToFile(String fileName, String filePath, String
	 * content) throws IOException, SftpException {
	 * 
	 * SFTPConnectionUtil lConnectionUtil = new SFTPConnectionUtil(); ChannelSftp
	 * lsftp = lConnectionUtil.getSFTPConnection();
	 * System.out.println("filePath::::"+filePath+" fileName:"+fileName );
	 * 
	 * //String fileName = responseDataMap.get("lFileCreationParam"); //String
	 * content = responseDataMap.get(fileName); lsftp.cd(filePath); InputStream
	 * stream = new ByteArrayInputStream((content.getBytes()));
	 * 
	 * lsftp.put(stream, filePath+fileName);
	 * 
	 * stream.close();
	 * 
	 * lConnectionUtil.closeConnection();
	 * 
	 * }
	 */

	private void readMT940FromAHBSantander(String incomingFilePathForMT940, ChannelSftp lsftp, Connection conn) throws IOException, SftpException {

		

		try {
		//	System.out.println(url+ userName+ password);
			
			
			System.out.println("filePath::::" + incomingFilePathForMT940 );
			
			lsftp.cd(incomingFilePathForMT940);

			Vector<ChannelSftp.LsEntry> list = lsftp.ls("*.txt");

			for (ChannelSftp.LsEntry entry : list) {
				System.out.println(entry.getFilename());
				if (entry.getFilename().toUpperCase().contains("MT940")) {
					File tempFile = new File(entry.getFilename());
					lsftp.get(entry.getFilename(), tempFile.getAbsolutePath()); // copying to local file

					try {
						MT940 mt940 = MT940.parse(tempFile);
//						System.out.println(" From : "+incomingFilePathForMT940 +"/" + entry.getFilename());
//						System.out.println(" To : "+ibos_to_Santander_Backup_path_MT940 + "/" + entry.getFilename());

						//persist
						MTXXXDao dao   = new MTXXXDao();
						 boolean flag =  dao.persistAccountDetailsFromMT940(mt940,conn);
						
						System.out.println("Data inserted successfully "+flag);
						System.out.println("Message : "+mt940.message());
						
						lsftp.rename(incomingFilePathForMT940 +"/" + entry.getFilename(),
								ibos_to_Santander_Backup_path_MT940 + "/" + entry.getFilename()+ new SimpleDateFormat("DDMMYYHHMMSS").format(new Date()));
						//lsftp.rm(incomingFilePathForMT940 + "/" + entry.getFilename());
						tempFile.delete();



					} catch (Exception e) {
//						System.out.println(" From : "+incomingFilePathForMT940 +"/" + entry.getFilename());
//						System.out.println(" To : "+ibos_to_Santander_Failed_path_MT940 + "/" + entry.getFilename());

						System.err.println("Error while parsing file " + entry.getFilename());
						
						lsftp.rename(incomingFilePathForMT940 +"/" + entry.getFilename(),
								ibos_to_Santander_Failed_path_MT940 + "/" + entry.getFilename()+ new SimpleDateFormat("DDMMYYHHMMSS").format(new Date()));
						//lsftp.rm(incomingFilePathForMT940 +"/"+ entry.getFilename());
						tempFile.delete();
					}

				}
				
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception Occured while processing MT940 message..." + e.getLocalizedMessage());

		}
	}

	private void readMT942FromAHBSantander(String incomingFilePathForMT942, ChannelSftp lsftp, Connection conn) throws IOException, SftpException {


		try {
		//	System.out.println(url+ userName+ password);
			
			System.out.println("filePath::::" + incomingFilePathForMT942 );
			
			lsftp.cd(incomingFilePathForMT942);

			Vector<ChannelSftp.LsEntry> list = lsftp.ls("*.txt");

			for (ChannelSftp.LsEntry entry : list) {
				System.out.println(entry.getFilename());
				if (entry.getFilename().toUpperCase().contains("MT942")) {
					File tempFile = new File(entry.getFilename());
					lsftp.get(entry.getFilename(), tempFile.getAbsolutePath()); // copying to local file

					try {
						MT942 mt942 = MT942.parse(tempFile);
//						System.out.println(" From : "+incomingFilePathForMT940 +"/" + entry.getFilename());
//						System.out.println(" To : "+ibos_to_Santander_Backup_path_MT940 + "/" + entry.getFilename());

						//persist
						MTXXXDao dao   = new MTXXXDao();
						 boolean flag =  dao.persistTxnDetailsFromMT942(mt942,"NA",conn);
						
						System.out.println("Data inserted successfully "+flag);
						System.out.println("Message : "+mt942.message());
						
						lsftp.rename(incomingFilePathForMT942 +"/" + entry.getFilename(),
								ibos_to_Santander_Backup_path_MT942 + "/" + entry.getFilename()+ new SimpleDateFormat("DDMMYYHHMMSS").format(new Date()));
						//lsftp.rm(incomingFilePathForMT940 + "/" + entry.getFilename());
						tempFile.delete();



					} catch (Exception e) {
//						System.out.println(" From : "+incomingFilePathForMT940 +"/" + entry.getFilename());
//						System.out.println(" To : "+ibos_to_Santander_Failed_path_MT940 + "/" + entry.getFilename());

						System.err.println("Error while parsing file " + entry.getFilename());
						
						lsftp.rename(incomingFilePathForMT942 +"/" + entry.getFilename(),
								ibos_to_Santander_Failed_path_MT942 + "/" + entry.getFilename()+ new SimpleDateFormat("DDMMYYHHMMSS").format(new Date()));
						//lsftp.rm(incomingFilePathForMT940 +"/"+ entry.getFilename());
						tempFile.delete();
					}

				}
				
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception Occured while processing MT940 message..." + e.getLocalizedMessage());

		}
	}
}
