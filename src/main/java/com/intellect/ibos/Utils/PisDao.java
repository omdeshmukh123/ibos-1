package com.intellect.ibos.Utils;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.intellect.ibos.DBConnectionUtil;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;

@Repository
public class PisDao {

	/**
	 * Instantiating logger for logging purpose
	 */
	private static final Logger logger = LoggerFactory.getLogger(PisDao.class);

	String initialStatus = "RCVI";

	@Value("${SENT_PAIN001_PATH}")
	private String sentPain001Path;
	
	@Value("${IBOS_TO_SAN}")
	private String ibosToSantander;

	@Value("${DB_url}")
	String url;

	@Value("${DB_username}")
	String userName;

	@Value("${DB_password}")
	String password;
	
	@Value("${TOMCAT_PATH_FAILED_FILES}")
	String localPath;

	private static ArrayList arrIbansCache = null;

	  
	public HashMap getAccessTokenInfo(String sAccountID, String url , String userName, String password) throws SQLException {
		PreparedStatement pst = null;
		DBConnectionUtil dbUtil = null;
		Connection conn = null;
		ResultSet rs = null;
		HashMap<String, Comparable> hmap = null;
		String query = "select b.acc_key, a.agreement_id, a.authorizer_id, "
				+ "EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP::timestamp - b.creation_date::timestamp)) as datediff "
				+ "from public.ibos_config_param_table a, public.ibos_init_params b WHERE "
				+ "a.param_value=? and "
				+ "a.agreement_id = b.agreement_id and "
				+ "a.authorizer_id = b.authorizer_id and b.acc_key <> 'WIP'";
		System.out.println("getAccessTokenInfo==>"+query);
		try {
			hmap = new HashMap<String, Comparable>();
			dbUtil = new DBConnectionUtil();
			conn = dbUtil.mGetConnection(url, userName, password);
			pst = conn.prepareStatement(query);
			pst.setString(1, sAccountID);
			rs = pst.executeQuery();
			while (rs.next()) {
				hmap.put("accessKey", rs.getString(1));
				hmap.put("agreementid", rs.getString(2));
				hmap.put("authid", rs.getString(3));
				hmap.put("datediff", rs.getInt(4));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(rs != null) {
			rs.close();
			}
			dbUtil.mCloseResources(pst, conn);
		}
		System.out.println("hmap"+hmap.toString());
		return hmap;

	}

	
	public int updateAuthAgreementId(String accessKey, String keycreationdt, String agreementid,
			 String authid, String url , String userName, String password) throws SQLException {
		PreparedStatement pst = null;
		DBConnectionUtil dbUtil = null;
		Connection conn = null;
		int i=0;
		String query = "update public.ibos_init_params set acc_key=?, "
				+ "creation_date=CURRENT_TIMESTAMP where agreement_id=? "
				+ "and authorizer_id=? and acc_key <> 'WIP'";
		System.out.println("updateAuthAgreementId:=="+query);
		try {
			dbUtil = new DBConnectionUtil();
			conn = dbUtil.mGetConnection(url, userName, password);
			pst = conn.prepareStatement(query);
			pst.setString(1, accessKey);
			pst.setString(2, agreementid);
			pst.setString(3, authid);

			i=pst.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbUtil.mCloseResources(pst, conn);
		}
		System.out.println("i:---"+i);
		return i;

	}


	
	/**
	 * Method to get Payment Initiation Request from Intesa
	 * 
	 * @param paymentInitReqJson
	 * @throws SQLException
	 * 
	 */
	public JSONObject getPaymentInitReqFromISPToNordea(JSONObject paymentInitReqJson, String accountHoldingBank, String authid,
			String url, String userName, String password, String paymentId, String holdingBank) throws SQLException {
		JSONObject convertedISPToNordeaJson = convertReqFromISPToNordea(paymentInitReqJson,paymentId, authid);
		persistPaymentId(paymentId, initialStatus, accountHoldingBank, url, userName, password, holdingBank);
		return convertedISPToNordeaJson;
	}

	/**
	 * Method to get Payment Initiation Request from PBZ
	 * 
	 * @param paymentInitReqJson
	 * @throws SQLException
	 * 
	 */
	public JSONObject getPaymentInitReqFromPBZToNordea(JSONObject paymentInitReqJson, String accountHoldingBank,
			String url, String userName, String password, String paymentId, String holdingBank, String authid) throws SQLException {
		JSONObject paymentAHBReqJson = convertReqFromPBZToNordea(paymentInitReqJson, paymentId, authid);
		persistPaymentId(paymentId, initialStatus, accountHoldingBank, url, userName, password, holdingBank);

		return paymentAHBReqJson;
	}

	/**
	 * Convert HB Request to AHB Request JSON
	 * 
	 * @param paymentInitReqJson
	 * @param paymentId
	 * @return paymentAHBReq
	 */
	public JSONObject convertReqFromISPToNordea(JSONObject paymentInitReqJson, String paymentId, String authid) {
		JSONObject creditorAccountObj = paymentInitReqJson.getJSONObject("creditorAccount");
		String creditorIban = creditorAccountObj.get("iban").toString();

		JSONObject debtorAccountObj = paymentInitReqJson.getJSONObject("debtorAccount");
		String debtorIban = debtorAccountObj.get("iban").toString();

		String creditorName = paymentInitReqJson.get("creditorName").toString();
		JSONObject instructedAmountObj = paymentInitReqJson.getJSONObject("instructedAmount");
		String currency = instructedAmountObj.get("currency").toString();
		String amount = instructedAmountObj.get("amount").toString();

		Map<String, Object> paymentAHBReqJson = new HashMap<>();
		paymentAHBReqJson.put("template_id", "SEPA_CREDIT_TRANSFER_FI");
		paymentAHBReqJson.put("authorizer_id", authid);
		paymentAHBReqJson.put("amount", amount);
		paymentAHBReqJson.put("currency", currency);
		paymentAHBReqJson.put("external_id", paymentId);
		String endToEndIdentification = paymentInitReqJson.get("endToEndIdentification").toString();
		paymentAHBReqJson.put("end_to_end_id", endToEndIdentification);

		HashMap<String, Object> creditor = new HashMap<String, Object>();
		HashMap<String, Object> account = new HashMap<String, Object>();
		account.put("value", creditorIban);
		account.put("type", "IBAN");

		if(paymentInitReqJson.has("remittanceInformationUnstructured")) {
			String message = paymentInitReqJson.get("remittanceInformationUnstructured").toString();
			creditor.put("message", message);
			}else {
			creditor.put("message", "Remittance Information");	
			}
		
		creditor.put("name", creditorName);
		creditor.put("account", account);
		paymentAHBReqJson.put("creditor", creditor);

		HashMap<String, Object> debtor = new HashMap<String, Object>();
		HashMap<String, Object> debtorAccount = new HashMap<String, Object>();
		debtorAccount.put("value", debtorIban);
		debtorAccount.put("type", "IBAN");
		debtorAccount.put("currency", currency);
		debtor.put("account", debtorAccount);
		debtor.put("own_reference", "Own notes");
		paymentAHBReqJson.put("debtor", debtor);
		JSONObject paymentAHBReq = new JSONObject(paymentAHBReqJson);
		return paymentAHBReq;
	}

	/**
	 * Convert HB Request to AHB Request JSON -PBZ TO Nordea
	 * 
	 * @param paymentInitReqJson
	 * @param paymentId
	 * @return paymentAHBReq
	 */
	public JSONObject convertReqFromPBZToNordea(JSONObject paymentInitReqJson, String paymentId, String authid) {
		JSONObject creditorAccountObj = paymentInitReqJson.getJSONObject("creditorAccount");
		String creditorIban = creditorAccountObj.get("iban").toString();

		JSONObject debtorAccountObj = paymentInitReqJson.getJSONObject("debtorAccount");
		String debtorIban = debtorAccountObj.get("iban").toString();

		String creditorName = paymentInitReqJson.get("creditorName").toString();
		JSONObject instructedAmountObj = paymentInitReqJson.getJSONObject("instructedAmount");
		String currency = instructedAmountObj.get("currency").toString();
		String amount = instructedAmountObj.get("amount").toString();

		Map<String, Object> paymentAHBReqJson = new HashMap<>();
		paymentAHBReqJson.put("template_id", "SEPA_CREDIT_TRANSFER_FI");
		paymentAHBReqJson.put("authorizer_id", authid);
		paymentAHBReqJson.put("amount", amount);
		paymentAHBReqJson.put("currency", currency);
		paymentAHBReqJson.put("external_id", paymentId);
		String endToEndIdentification = paymentInitReqJson.get("endToEndIdentification").toString();
		paymentAHBReqJson.put("end_to_end_id", endToEndIdentification);

		HashMap<String, Object> creditor = new HashMap<String, Object>();
		HashMap<String, Object> account = new HashMap<String, Object>();
		account.put("value", creditorIban);
		account.put("type", "IBAN");

		
		if(paymentInitReqJson.has("remittanceInformationUnstructured")) {
		String message = paymentInitReqJson.get("remittanceInformationUnstructured").toString();
		creditor.put("message", message);
		}else {
		creditor.put("message", "Remittance Information");	
		}
		
		creditor.put("name", creditorName);
		creditor.put("account", account);
		
		paymentAHBReqJson.put("creditor", creditor);

		HashMap<String, Object> debtor = new HashMap<String, Object>();
		HashMap<String, Object> debtorAccount = new HashMap<String, Object>();
		debtorAccount.put("value", debtorIban);
		debtorAccount.put("type", "IBAN");
		debtorAccount.put("currency", currency);
		debtor.put("account", debtorAccount);
		debtor.put("own_reference", "Own notes");
		paymentAHBReqJson.put("debtor", debtor);
		JSONObject paymentAHBReq = new JSONObject(paymentAHBReqJson);
		return paymentAHBReq;
	}

	private boolean persistPaymentId(String paymentId, String status, String bankCode, String url, String userName,
			String password, String holdingBank) throws SQLException {
		DBConnectionUtil dbUtil = null;
		PreparedStatement pst = null;
		Connection conn = null;
		boolean output = false;
		String query = "INSERT INTO public.ibos_payment_init (payment_id_ig,bank_code,transaction_status,hb_bank_code,ahb_txn_status,creation_datetime) values(?,?,?,?,?,CURRENT_TIMESTAMP)";
		System.out.println(query+"\n 1. paymentId:"+paymentId+"-"+bankCode+"-"+status+"-"+holdingBank);
		try {
			dbUtil = new DBConnectionUtil();
			conn = dbUtil.mGetConnection(url, userName, password);
			pst = conn.prepareStatement(query);
			pst.setString(1, paymentId);
			pst.setString(2, bankCode);
			pst.setString(3, status);
			pst.setString(4, holdingBank);
			pst.setString(5, status);
			pst.execute();
			System.out.println(pst);
			output = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbUtil.mCloseResources(pst, conn);

		}
		return output;

	}

	public JSONObject IdentifyAHBFromDB(String idalPaymentId, String url, String userName, String password)
			throws SQLException {
		JSONObject ahbBankInfo = getBankCodeFromDB(idalPaymentId, url, userName, password);

		return ahbBankInfo;
	}

	public JSONObject getBankCodeFromDB(String idalPaymentId, String url, String userName, String password)
			throws SQLException {
		PreparedStatement pst = null;
		DBConnectionUtil dbUtil = null;
		Connection conn = null;
		ResultSet rs = null;
		String bankCode = null;
		String ahbPayId = null;
		String hbBankCode = null;
		JSONObject ahbInfo = new JSONObject();
		String query = "select bank_code , payment_id_ahb ,hb_bank_code from public.ibos_payment_init where payment_id_ig = ? and hb_bank_code is not null and bank_code is not null";
		System.out.println(query);
		try {
			dbUtil = new DBConnectionUtil();
			conn = dbUtil.mGetConnection(url, userName, password);
			pst = conn.prepareStatement(query);
			pst.setString(1, idalPaymentId);
			rs = pst.executeQuery();
			while (rs.next()) {
				bankCode = rs.getString(1);
				ahbPayId = rs.getString(2);
				hbBankCode = rs.getString(3);
				ahbInfo.accumulate("bankCode", bankCode);
				ahbInfo.accumulate("ahbPayId", ahbPayId);
				ahbInfo.accumulate("hbBankCode", hbBankCode);
				System.out.println(pst);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				rs.close();
			}
			dbUtil.mCloseResources(pst, conn);
			conn.close();
		}
		return ahbInfo;

	}

	public void PersistPaymentInitReq(String paymentId, JSONObject paymentInitReq, String accountHoldingBank,
			String url, String userName, String password, String holdingBank) throws SQLException {
		persistPaymentId(paymentId, initialStatus, accountHoldingBank, url, userName, password, holdingBank);

	}

	public JSONObject convNortoIntesaJsonForPay(String responseMesage, String paymentId, String url, String userName,
			String password) {
		JSONObject intessaResponse = new JSONObject();
		JSONObject nordeaResponse = new JSONObject(responseMesage);
		JSONObject response = nordeaResponse.getJSONObject("response");
		String paymentStatus = response.get("payment_status").toString();
		if (paymentStatus.equals("AUTHORIZATION_PENDING")) {
			intessaResponse.put("transactionStatus", "RCVI");
		} else if (paymentStatus.equals("PAYMENT_EXECUTED")) {
			intessaResponse.put("transactionStatus", "ACSC");
		} else if (paymentStatus.equals("PAYMENT_REJECTED")) {
			intessaResponse.put("transactionStatus", "RJCT");
		}else if (paymentStatus.equals("PAYMENT_ACCEPTED")) {
			intessaResponse.put("transactionStatus", "ACSP");
		}
		String ahbPayId = response.get("_id").toString();
		persistAhbPayId(paymentId, paymentStatus, ahbPayId, url, userName, password);
		intessaResponse.put("paymentId", paymentId);
		return intessaResponse;
	}

	private boolean persistAhbPayId(String paymentId, String status, String ahbPayId, String url, String userName,
			String password) {
		DBConnectionUtil dbUtil = null;
		PreparedStatement pst = null;
		Connection conn = null;
		boolean output = false;
		String query = "UPDATE public.ibos_payment_init SET payment_id_ahb = ? , transaction_status = ? where payment_id_ig = ? ";
		System.out.println(query);
		try {
			dbUtil = new DBConnectionUtil();
			conn = dbUtil.mGetConnection(url, userName, password);
			pst = conn.prepareStatement(query);
			pst.setString(1, ahbPayId);
			pst.setString(2, status);
			pst.setString(3, paymentId);
			pst.execute();
			System.out.println(pst);
			output = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// pst.close();
			dbUtil.mCloseResources(pst, conn);

		}
		return output;
	}

	public Object convPBZtoISPJsonForPay(String responseMesage, String paymentId, String url, String userName,
			String password) {
		JSONObject ispResponse = new JSONObject();
		JSONObject pbzResponse = new JSONObject(responseMesage);
		String paymentStatus = pbzResponse.get("transactionStatus").toString();

		String ahbPayId = pbzResponse.get("paymentId").toString();
		persistAhbPayId(paymentId, paymentStatus, ahbPayId, url, userName, password);
		ispResponse.put("paymentId", paymentId);
		ispResponse.put("transactionStatus", paymentStatus);
		return ispResponse;
	}

	public Object convISPtoPBZJsonForPay(String responseMesage, String paymentId, String url, String userName,
			String password) {
		JSONObject pbzResponse = new JSONObject();
		JSONObject ispResponse = new JSONObject(responseMesage);
		String paymentStatus = ispResponse.get("transactionStatus").toString();

		String ahbPayId = ispResponse.get("paymentId").toString();
		persistAhbPayId(paymentId, paymentStatus, ahbPayId, url, userName, password);
		pbzResponse.put("paymentId", paymentId);
		pbzResponse.put("transactionStatus", paymentStatus);
		return pbzResponse;
	}

	public Object convNorToPBZJsonForPay(String responseMesage, String paymentId, String url, String userName,
			String password) {
		JSONObject pbzResponse = new JSONObject();
		JSONObject nordeaResponse = new JSONObject(responseMesage);
		JSONObject response = nordeaResponse.getJSONObject("response");
		String paymentStatus = response.get("payment_status").toString();
		if (paymentStatus.equals("AUTHORIZATION_PENDING")) {
			pbzResponse.put("transactionStatus", "RCVI");
		} else if (paymentStatus.equals("PAYMENT_EXECUTED")) {
			pbzResponse.put("transactionStatus", "ACSC");
		} else if (paymentStatus.equals("PAYMENT_REJECTED")) {
			pbzResponse.put("transactionStatus", "RJCT");
		}else if (paymentStatus.equals("PAYMENT_ACCEPTED")) {
			pbzResponse.put("transactionStatus", "ACSP");
		}

		String ahbPayId = response.get("_id").toString();
		persistAhbPayId(paymentId, paymentStatus, ahbPayId, url, userName, password);
		pbzResponse.put("paymentId", paymentId);
		return pbzResponse;
	}

	public Object convNortoIntesaJsonForPayStatus(String idalPaymentId,String responseMesage ,String url, String userName,
			String password) {
		JSONObject intesaResponse = new JSONObject();
		JSONObject nordeaResponse = new JSONObject(responseMesage);
		JSONObject responseObj = nordeaResponse.getJSONObject("response");
		String paymentStatus = responseObj.get("payment_status").toString();
		// stub its configured as AUTHORIZATION_PENDING
		if (paymentStatus.equals("AUTHORIZATION_PENDING")) {
			intesaResponse.put("transactionStatus", "ACSP");
		} else if (paymentStatus.equals("PAYMENT_EXECUTED")) {
			intesaResponse.put("transactionStatus", "ACSC");
		} else if (paymentStatus.equals("PAYMENT_REJECTED")) {
			intesaResponse.put("transactionStatus", "RJCT");
		}else if (paymentStatus.equals("PAYMENT_ACCEPTED")) {
			intesaResponse.put("transactionStatus", "ACSP");
		}
		persistGetStatus(idalPaymentId, paymentStatus, url, userName, password);
		return intesaResponse;
	}

	public void persistGetStatus(String idalPaymentId, String paymentStatus, String url, String userName,
			String password) {
		DBConnectionUtil dbUtil = null;
		PreparedStatement pst = null;
		Connection conn = null;
		boolean output = false;
		String query = "UPDATE public.ibos_payment_init SET transaction_status = ? where payment_id_ig = ? ";
		System.out.println(query);
		try {
			dbUtil = new DBConnectionUtil();
			conn = dbUtil.mGetConnection(url, userName, password);
			pst = conn.prepareStatement(query);
			pst.setString(1, paymentStatus);
			pst.setString(2, idalPaymentId);	
			pst.execute();
			System.out.println(pst);
			output = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// pst.close();
			dbUtil.mCloseResources(pst, conn);

		}	
	}


	public Object convIsptoPbzJsonForPayStatus(String idalPaymentId,String responseMesage ,String url, String userName,
			String password) {
		JSONObject pbzResponse = new JSONObject();
		JSONObject intesaResponse = new JSONObject(responseMesage);
		String paymentStatus = intesaResponse.get("transactionStatus").toString();
		pbzResponse.put("transactionStatus", paymentStatus);
		persistGetStatus(idalPaymentId, paymentStatus, url, userName, password);
		return pbzResponse;
	}

	public Object convPbztoIspJsonForPayStatus(String idalPaymentId,String responseMesage ,String url, String userName,
			String password) {
		JSONObject intesaResponse = new JSONObject();
		JSONObject pbzResponse = new JSONObject(responseMesage);
		String paymentStatus = pbzResponse.get("transactionStatus").toString();
		intesaResponse.put("transactionStatus", paymentStatus);
		persistGetStatus(idalPaymentId, paymentStatus, url, userName, password);
		return intesaResponse;
	}

	public Object convNortoPbzJsonForPayStatus(String idalPaymentId,String responseMesage ,String url, String userName,
			String password) {
		JSONObject pbzResponse = new JSONObject();
		JSONObject nordeaResponse = new JSONObject(responseMesage);
		JSONObject responseObj = nordeaResponse.getJSONObject("response");
		String paymentStatus = responseObj.get("payment_status").toString();
		if (paymentStatus.equals("AUTHORIZATION_PENDING")) {
			pbzResponse.put("transactionStatus", "RCVI");
		} else if (paymentStatus.equals("PAYMENT_EXECUTED")) {
			pbzResponse.put("transactionStatus", "ACSC");
		} else if (paymentStatus.equals("PAYMENT_REJECTED")) {
			pbzResponse.put("transactionStatus", "RJCT");
		}else if (paymentStatus.equals("PAYMENT_ACCEPTED")) {
			pbzResponse.put("transactionStatus", "ACSP");
		}
		persistGetStatus(idalPaymentId, paymentStatus, url, userName, password);
		return pbzResponse;
	}

	public void getPaymentInitReqFromSanToNordea(String accountHoldingBank,
			String url, String userName, String password, String paymentId, String holdingBank) throws SQLException {
		
		persistPaymentId(paymentId, initialStatus, accountHoldingBank, url, userName, password, holdingBank);
	
	}

	public Object convNortoSanJsonForPay(String responseMesage, String paymentId, String url, String userName,
			String password) throws ParserConfigurationException, TransformerException, IOException, SftpException {
		JSONObject norToSanResponse = new JSONObject();
		JSONObject nordeaResponse = new JSONObject(responseMesage);
		JSONObject response = nordeaResponse.getJSONObject("response");
		String paymentStatus = response.get("payment_status").toString();
		if ( paymentStatus.equals("AUTHORIZATION_PENDING")) {
			norToSanResponse.put("transactionStatus", "RCVI");
		} else if (paymentStatus.equals("PAYMENT_EXECUTED")) {
			norToSanResponse.put("transactionStatus", "ACSC");
		} else if (paymentStatus.equals("PAYMENT_REJECTED")) {
			norToSanResponse.put("transactionStatus", "RJCT");
		}else if (paymentStatus.equals("PAYMENT_ACCEPTED")) {
			norToSanResponse.put("transactionStatus", "ACSP");
		}
		String ahbPayId = response.get("_id").toString();

		persistAhbPayId(paymentId, paymentStatus, ahbPayId, url, userName, password);
		norToSanResponse.put("paymentId", paymentId);
		if (logger.isInfoEnabled()) {
			logger.info("Converted ISP to SAN JSON------->NOR TO SAN<-------"+ norToSanResponse);
		}
		return norToSanResponse;

	}

	public Object convPBZToSanJsonForPay(String responseMesage, String paymentId, String url, String userName,
			String password) throws IOException, ParserConfigurationException, TransformerException, SftpException {
		JSONObject pbztoSanResponse = new JSONObject();
		JSONObject pbzResponse = new JSONObject(responseMesage);
		String paymentStatus = pbzResponse.get("transactionStatus").toString();
		pbztoSanResponse.put("transactionStatus", paymentStatus);
		String ahbPayId = pbzResponse.get("paymentId").toString();
		persistAhbPayId(paymentId, paymentStatus, ahbPayId, url, userName, password);
		pbztoSanResponse.put("paymentId", paymentId);
		if (logger.isInfoEnabled()) {
			logger.info("Converted PBZ to SAN JSON------->PBZ TO SAN<-------"+ pbztoSanResponse);
		}
		return pbztoSanResponse;

	}

	public Object convISPtoSanJsonForPay(String responseMesage, String paymentId, String url, String userName,
			String password) throws ParserConfigurationException, TransformerException, IOException, SftpException {
		JSONObject isptoSanResponse = new JSONObject(responseMesage);
		JSONObject ispResponse = new JSONObject(responseMesage);
		String paymentStatus = ispResponse.get("transactionStatus").toString();
		isptoSanResponse.put("transactionStatus", paymentStatus);
		String ahbPayId = ispResponse.get("paymentId").toString();
		persistAhbPayId(paymentId, paymentStatus, ahbPayId, url, userName, password);
		isptoSanResponse.put("paymentId", paymentId);
		if (logger.isInfoEnabled()) {
			logger.info("Converted ISP to SAN JSON------->ISP TO SAN<-------"+ isptoSanResponse);
		}
		return isptoSanResponse;
		
		
	}

	public List<String> getPendingStatusFromDB(String url, String userName, String password) throws SQLException {
		PreparedStatement pst = null;
		DBConnectionUtil dbUtil = null;
		Connection conn = null;
		ResultSet rs = null;
		String pendingPayId = null;
		List<String> pendingStatusPayId = new ArrayList<>();
		String query = "select payment_id_ig from public.ibos_payment_init where transaction_status NOT IN (?,?) and hb_bank_code ='SAN' and count_pain002 <=2";
		System.out.println(query);
		try {
			dbUtil = new DBConnectionUtil();
			conn = dbUtil.mGetConnection(url, userName, password);
			pst = conn.prepareStatement(query);
			pst.setString(1, "PAYMENT_REJECTED");
			pst.setString(2, "PAYMENT_EXECUTED");
			rs = pst.executeQuery();
			System.out.println(pst);
			while (rs.next()) {
				pendingPayId = rs.getString(1);
				pendingStatusPayId.add(pendingPayId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				rs.close();
			}
			dbUtil.mCloseResources(pst, conn);
		}
		return pendingStatusPayId;

	}

	public Object convNortoSanJsonForPayStatus(String responseMesage, String idalPaymentId,String url, String userName,String password) {
		JSONObject santanderResponse = new JSONObject();
		JSONObject nordeaResponse = new JSONObject(responseMesage);
		JSONObject responseObj = nordeaResponse.getJSONObject("response");
		String paymentStatus = responseObj.get("payment_status").toString();
		// stub its configured as AUTHORIZATION_PENDING
		if (paymentStatus.equals("AUTHORIZATION_PENDING")) {
			santanderResponse.put("transactionStatus", "ACSP");
		} else if (paymentStatus.equals("PAYMENT_EXECUTED")) {
			santanderResponse.put("transactionStatus", "ACSC");
		} else if (paymentStatus.equals("PAYMENT_REJECTED")) {
			santanderResponse.put("transactionStatus", "RJCT");
		}else if (paymentStatus.equals("PAYMENT_ACCEPTED")) {
			santanderResponse.put("transactionStatus", "ACSP");
		}
		persistGetStatus(idalPaymentId, paymentStatus, url, userName, password);
		return santanderResponse;
	}

	public Object convIsptoSanJsonForPayStatus(String responseMesage, String idalPaymentId,String url, String userName,String password) {

		JSONObject santanderResponse = new JSONObject();
		JSONObject intesaResponse = new JSONObject(responseMesage);
		String paymentStatus = intesaResponse.get("transactionStatus").toString();
		santanderResponse.put("transactionStatus", paymentStatus);
		persistGetStatus(idalPaymentId, paymentStatus, url, userName, password);
		return santanderResponse;
	}

	public Object convPbztoSanJsonForPayStatus(String responseMesage, String idalPaymentId,String url, String userName,String password) {

		JSONObject santanderResponse = new JSONObject();
		JSONObject pbzResponse = new JSONObject(responseMesage);
		String paymentStatus = pbzResponse.get("transactionStatus").toString();
		santanderResponse.put("transactionStatus", paymentStatus);
		System.out.println("convPbztoSanJsonForPayStatus payment Status------------------->" + paymentStatus);
		persistGetStatus(idalPaymentId, paymentStatus, url, userName, password);
		return santanderResponse;
	}

	public void convertHBtoSanAHB(JSONObject paymentInitReqJson, String requestDate, String paymentId,String InitgPty)
			throws SftpException, ParserConfigurationException, TransformerException, IOException ,SQLException{

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document document = dBuilder.newDocument();
		
		Element doc = document.createElement("Document");
		Attr xmlns = document.createAttribute("xmlns");
		xmlns.setValue("urn:iso:std:iso:20022:tech:xsd:pain.001.001.03");
		doc.setAttributeNode(xmlns);
		document.appendChild(doc);
		
		Element cstmrCdtTrfInitn = document.createElement("CstmrCdtTrfInitn");
		doc.appendChild(cstmrCdtTrfInitn);
		Element grpHdr = document.createElement("GrpHdr");
		cstmrCdtTrfInitn.appendChild(grpHdr);
		
		Element msgId = document.createElement("MsgId");
		msgId.appendChild(document.createTextNode(paymentId));
		grpHdr.appendChild(msgId);

		Element creDtTm = document.createElement("CreDtTm");
		Element reqdExctnDt = document.createElement("ReqdExctnDt");
		Date currentDate = new Date();
		String sdf = convertToGMTDate(currentDate, "YYYY-MM-dd'T'hh:mm:ss.sss", "GMT");
		creDtTm.appendChild(document.createTextNode(sdf));
		grpHdr.appendChild(creDtTm);

		Element nbOfTxs = document.createElement("NbOfTxs");
		nbOfTxs.appendChild(document.createTextNode("1"));
		grpHdr.appendChild(nbOfTxs);

	
		Element initgPty = document.createElement("InitgPty");
		Element nm = document.createElement("Nm");
		initgPty.appendChild(nm);
		if("PBZGHR".equals(InitgPty)) {
			InitgPty="PBZ";
		}
		nm.appendChild(document.createTextNode(InitgPty)); 	
		grpHdr.appendChild(initgPty);

		Element pmtInf = document.createElement("PmtInf");
		cstmrCdtTrfInitn.appendChild(pmtInf);

		Element pmtInfId = document.createElement("PmtInfId");
		UUID uuid = UUID.randomUUID();
		long l = ByteBuffer.wrap(uuid.toString().getBytes()).getLong();
		String uniqueGenMsgId =  Long.toString(l, Character.MAX_RADIX);
		pmtInfId.appendChild(document.createTextNode(uniqueGenMsgId));
		pmtInf.appendChild(pmtInfId);

		Element pmtMtd = document.createElement("PmtMtd");
		pmtMtd.appendChild(document.createTextNode("TRF"));
		pmtInf.appendChild(pmtMtd);

		Element pmtTpInf = document.createElement("PmtTpInf");
		Element svcLvl = document.createElement("SvcLvl");
		Element cd = document.createElement("Cd");
		cd.appendChild(document.createTextNode("SEPA"));
		svcLvl.appendChild(cd);
		pmtTpInf.appendChild(svcLvl);
		pmtInf.appendChild(pmtTpInf);
		
		String sdfDate = convertToGMTDate(currentDate, "YYYY-MM-dd", "GMT");
		reqdExctnDt.appendChild(document.createTextNode(sdfDate));
		pmtInf.appendChild(reqdExctnDt);
		Element dbtr = document.createElement("Dbtr");
		Element dbtrNm = document.createElement("Nm");
		dbtr.appendChild(dbtrNm);
		dbtrNm.appendChild(document.createTextNode("Debtor Name"));
		pmtInf.appendChild(dbtr);

		Element dbtrAcct = document.createElement("DbtrAcct");
		pmtInf.appendChild(dbtrAcct);
		
		Element dbtrAgt = document.createElement("DbtrAgt");
		Element finInstnId = document.createElement("FinInstnId");
		Element bic = document.createElement("BIC");
		bic.appendChild(document.createTextNode("BSCHDEFFXXX"));
		finInstnId.appendChild(bic);
		dbtrAgt.appendChild(finInstnId);
		pmtInf.appendChild(dbtrAgt);

		Element id = document.createElement("Id");
		Element iBAN = document.createElement("IBAN");
		JSONObject debtorAccount = paymentInitReqJson.getJSONObject("debtorAccount");
		String iban = debtorAccount.get("iban").toString();
		iBAN.appendChild(document.createTextNode(iban)); 
		dbtrAcct.appendChild(id);
		id.appendChild(iBAN);

		String endToEndIdentification = paymentInitReqJson.get("endToEndIdentification").toString();
		Element cdtTrfTxInf = document.createElement("CdtTrfTxInf");
		pmtInf.appendChild(cdtTrfTxInf);

		Element pmtId = document.createElement("PmtId");
		cdtTrfTxInf.appendChild(pmtId);
		Element endToEndId = document.createElement("EndToEndId");
		endToEndId.appendChild(document.createTextNode(endToEndIdentification));
		pmtId.appendChild(endToEndId);
		
		JSONObject instructedAmount = paymentInitReqJson.getJSONObject("instructedAmount");
		String amount = instructedAmount.get("amount").toString();
		
		Element amt = document.createElement("Amt");
		Element instdAmt = document.createElement("InstdAmt");
		Attr attr = document.createAttribute("Ccy");
		attr.setValue("EUR");
		instdAmt.setAttributeNode(attr);
		instdAmt.appendChild(document.createTextNode(amount));
		amt.appendChild(instdAmt);
		cdtTrfTxInf.appendChild(amt);
		
		String creditorName = paymentInitReqJson.get("creditorName").toString();
		Element Cdtr = document.createElement("Cdtr");
		Element Nm = document.createElement("Nm");
		Nm.appendChild(document.createTextNode(creditorName));
		Cdtr.appendChild(Nm);
		cdtTrfTxInf.appendChild(Cdtr);
		pmtInf.appendChild(cdtTrfTxInf);		
		cstmrCdtTrfInitn.appendChild(pmtInf);
		
		Element cdtrAcct = document.createElement("CdtrAcct");
		cdtTrfTxInf.appendChild(cdtrAcct);
		
		Element credId = document.createElement("Id");
		Element credIBAN = document.createElement("IBAN");
		JSONObject creditorAccount = paymentInitReqJson.getJSONObject("creditorAccount");
		String creditorAcctIban = creditorAccount.get("iban").toString();
		credIBAN.appendChild(document.createTextNode(creditorAcctIban));
		cdtrAcct.appendChild(credId);
		credId.appendChild(credIBAN);
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(document);

		StreamResult consoleResult = new StreamResult(System.out);
		transformer.transform(source, consoleResult);
		
		StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(doc), new StreamResult(writer));
        String output = writer.getBuffer().toString();
        
        if (logger.isInfoEnabled()) {
			logger.info("Transformed Pain001-------------------" + output);
		}
        
        String fileName = generateFileNameDtl();
		pushFiles(fileName ,output);
	}

	private void pushFiles(String fileName,String source) throws SftpException {
		SFTPConnectionUtilForPushingFiles lConnectionUtil = null;
		ChannelSftp lsftp = null;
		try {
			lConnectionUtil = new SFTPConnectionUtilForPushingFiles();
			lsftp = lConnectionUtil.getSFTPConnectionforPushingFiles();
			try {
			copyMessageToFile(fileName, source, lsftp);
			}
			catch(Exception e ) {
				System.out.println("Exception occured while copying String fileName,String source pain 002 to santander server "+e);
				copyMessagetoTomcatServer(fileName, source);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			//if( lsftp != null) {
				lConnectionUtil.closeConnection();
			//	}		
		}
		
	}

	private String generateFileNameDtl() throws SQLException {
		String num = fileNameSequenceGenerator(url, userName, password);
		//IPHSAN.AHBIGTB.I.PAIN01.0003.DAAMMDD.THHMM.NNNNNNNN
    	  String temp = "IPHSAN"+ "."+"AHBIGTB" + "." + "I" +  "." + "PAIN01"+  "." + "0003" +
    	  "."+ "D"+ new SimpleDateFormat("YYMMdd").format(new Date())+"."+"T"+ new SimpleDateFormat("hhmm").format(new Date())+ "." +num;
    				System.out.println(" File Name :" + temp);
			return temp;
	}
	
	public String generatDateSequence(String incomingPain001Path) throws SQLException {
		String IncomingPain001seq=null;
		if(incomingPain001Path.contains("."))
		{
	   String[] splitIncomingPain001Path=incomingPain001Path.split("\\.");  
	       IncomingPain001seq=splitIncomingPain001Path[6].concat(".").concat(splitIncomingPain001Path[7]);
	       
	     System.out.println("generated date && Sequence from incoming :"+IncomingPain001seq);
	     
	     
	}
		return IncomingPain001seq;
	}
	
	public void fileNameDateSequence(String paymentId,String url, String userName, String password) throws SQLException {
		PreparedStatement pst = null;
		DBConnectionUtil dbUtil = null;
		Connection conn = null;
		ResultSet rs = null;
		
		
		String query = "UPDATE public.ibos_payment_init SET pain001_date_seq = 0 where payment_id_ig = ? ";
		try {
			dbUtil = new DBConnectionUtil();
			conn = dbUtil.mGetConnection(url, userName, password);
			pst = conn.prepareStatement(query);
			//pst.setString(1,date_seq);
			pst.setString(1,paymentId);	
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				rs.close();
			}
			dbUtil.mCloseResources(pst, conn);
			System.out.println(pst);
		}
	
		
	}
	
	public void updatedDateSequence(String paymentId,String date_seq,String url, String userName, String password) throws SQLException {
		PreparedStatement pst = null;
		DBConnectionUtil dbUtil = null;
		Connection conn = null;
		ResultSet rs = null;
		
		
		String query = "UPDATE public.ibos_payment_init SET pain001_date_seq = ? where payment_id_ig = ? ";
		try {
			dbUtil = new DBConnectionUtil();
			conn = dbUtil.mGetConnection(url, userName, password);
			pst = conn.prepareStatement(query);
			pst.setString(1,date_seq);
			pst.setString(2,paymentId);
			
			pst.execute();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				rs.close();
			}
			dbUtil.mCloseResources(pst, conn);
			System.out.println(pst);
		}
	
		
	}

	private void copyMessageToFile(String fileName, String document, ChannelSftp lsftp)
			throws SftpException, IOException {

		System.out.println("filePath::::" + sentPain001Path + " fileName:" + fileName);
		lsftp.cd(sentPain001Path);
		InputStream stream = new ByteArrayInputStream((document.getBytes()));
		lsftp.put(stream, sentPain001Path + fileName);

		stream.close();

	}

	public String getPayStatusFromDB(String idalPaymentId, String url, String userName,
			String password) throws SQLException {
		PreparedStatement pst = null;
		DBConnectionUtil dbUtil = null;
		Connection conn = null;
		ResultSet rs = null;
		String paymentStatus = null;
		String query = "select ahb_txn_status from public.ibos_payment_init where payment_id_ig = ? order by creation_datetime ASC";
		System.out.println(query);
		try {
			dbUtil = new DBConnectionUtil();
			conn = dbUtil.mGetConnection(url, userName, password);
			pst = conn.prepareStatement(query);
			pst.setString(1, idalPaymentId);
			rs = pst.executeQuery();
			System.out.println(pst);
			while (rs.next()) {
				paymentStatus = rs.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				rs.close();
			}
			dbUtil.mCloseResources(pst, conn);
			System.out.println(pst);
		}
		return paymentStatus;

	}

	public Object convSantoPBZJsonForPayStatus(String idalPaymentId ,String txnstatus, String url, String userName ,String password ) {

		JSONObject santoPbzResponse = new JSONObject();
		santoPbzResponse.put("transactionStatus", txnstatus);
		//persistGetStatus(idalPaymentId, txnstatus, url, userName, password);
		return santoPbzResponse;
	}

	public Object convSantoISPJsonForPayStatus(String idalPaymentId ,String txnstatus, String url, String userName ,String password) {

		JSONObject santoIspResponse = new JSONObject();
		santoIspResponse.put("transactionStatus", txnstatus);
		//persistGetStatus(idalPaymentId, txnstatus, url, userName, password);
		return santoIspResponse;
	}

	public Object generatePayIdresponse(String paymentId) {
		JSONObject response = new JSONObject();
		response.put("paymentId", paymentId);
		response.put("transactionStatus", "RCVI");
		return response;	
	}

	public void insertPain002xmlFromSan(String OrgnlMsgId,String orgnlPmtInfAndSts,String txSts, String url, String userName,
			String password) {
		DBConnectionUtil dbUtil = null;
		PreparedStatement pst = null;
		Connection conn = null;
		String query = "INSERT INTO public.ibos_payment_init (payment_id_ig,orgnl_mssg_iden,ahb_txn_status,creation_datetime) values(?,?,?,CURRENT_TIMESTAMP)";
		System.out.println(query);
		try {
			dbUtil = new DBConnectionUtil();
			conn = dbUtil.mGetConnection(url, userName, password);
			pst = conn.prepareStatement(query);
			pst.setString(1, OrgnlMsgId);
			pst.setString(2, orgnlPmtInfAndSts);
			pst.setString(3, txSts);
			
			pst.execute();
			System.out.println(pst);
			System.out.println("From PainReader to insertPain002xmlFromSan"+ pst);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbUtil.mCloseResources(pst, conn);

		}
		
	}

	
	public void storePain001FromSantoAHB(String paymentId, String uniqueGenMsgId, String debtoriBAN, String msgId, String pmtInfId,
			String endToEndId,String instrId,String url, String userName,
			String password) {
		DBConnectionUtil dbUtil = null;
		PreparedStatement pst = null;
		Connection conn = null;
		String query = "UPDATE public.ibos_payment_init SET mssg_iden = ? , debtoraccount= ? ,orgnl_mssg_iden_name= ? ,payment_info_iden= ? ,instruction_id=?,end_to_end_iden=?,count_pain002 =1 where payment_id_ig = ?";
		System.out.println(query);
		try {
			dbUtil = new DBConnectionUtil();
			conn = dbUtil.mGetConnection(url, userName, password);
			pst = conn.prepareStatement(query);
			pst.setString(1, uniqueGenMsgId);
			pst.setString(2, debtoriBAN);
			pst.setString(3, msgId);	
			pst.setString(4, pmtInfId);
			pst.setString(5, instrId);
			pst.setString(6, endToEndId);
			pst.setString(7, paymentId);
			System.out.println(pst);
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbUtil.mCloseResources(pst, conn);

		}
		
	}

//	//payment_id_ig,mssg_iden,debtoraccount,orgnl_mssg_iden_name,payment_info_iden
//	public JSONObject fetchPain001DtlsFromDB(String paymentId, String url, String userName, String password) throws SQLException {
//		PreparedStatement pst = null;
//		DBConnectionUtil dbUtil = null;
//		Connection conn = null;
//		ResultSet rs = null;
//		String paymentStatus = null;
//		JSONObject response = new JSONObject();
//		String query = "select pmtInfId, debtoraccount, orgnl_mssg_iden_name from public.ibos_payment_init where payment_id_ahb = ?";
//		System.out.println(query);
//		try {
//			dbUtil = new DBConnectionUtil();
//			conn = dbUtil.mGetConnection(url, userName, password);
//			pst = conn.prepareStatement(query);
//			pst.setString(1, paymentId);
//			rs = pst.executeQuery();
//			while (rs.next()) {
//			
//				String pmtInfId = rs.getString(1);
//				String debtorIban = rs.getString(2);
//				String msgId = rs.getString(3);
//				response.put("pmtInfId",pmtInfId );
//				response.put("debtorIban",debtorIban);
//				response.put("msgId",msgId);
//				if (logger.isInfoEnabled()) {
//					logger.info("Response for fetching Pain001 Dtls FromDB :-------------------" + response);
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			if (rs != null) {
//				rs.close();
//			}
//			dbUtil.mCloseResources(pst, conn);
//		}
//		return response;
//
//		
//	}

	//transaction_status
	public JSONObject getPain001FromDBForPain002(String idalPaymentId, String url, String userName, String password)
			throws SQLException {
		PreparedStatement pst = null;
		DBConnectionUtil dbUtil = null;
		Connection conn = null;
		ResultSet rs = null;
		JSONObject fetchedPain001Dtls = new JSONObject();
		String query = "select mssg_iden, debtoraccount, orgnl_mssg_iden_name ,transaction_status,payment_info_iden,end_to_end_iden,instruction_id from public.ibos_payment_init where payment_id_ig = ?";
		System.out.println(query);
		try {
			dbUtil = new DBConnectionUtil();
			conn = dbUtil.mGetConnection(url, userName, password);
			pst = conn.prepareStatement(query);
			pst.setString(1, idalPaymentId);
			rs = pst.executeQuery();
			while (rs.next()) {
				fetchedPain001Dtls.put("uniqueGenMsgId", rs.getString(1));
				fetchedPain001Dtls.put("debtoriBAN", rs.getString(2));
				fetchedPain001Dtls.put("msgId", rs.getString(3));
				fetchedPain001Dtls.put("status", rs.getString(4));
				fetchedPain001Dtls.put("pmtInfId", rs.getString(5));
				fetchedPain001Dtls.put("endToEndId", rs.getString(6));
				fetchedPain001Dtls.put("instrId", rs.getString(7));
			}
			System.out.println(pst);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				rs.close();
			}
			dbUtil.mCloseResources(pst, conn);
			conn.close();
		}
		return fetchedPain001Dtls;

	}


	public List validateIncomingIbanWithDB(String url, String userName, String password) throws SQLException {
		if(arrIbansCache==null) {
			PreparedStatement pst = null;
			DBConnectionUtil dbUtil = null;
			Connection conn = null;
			ResultSet rs = null;
			arrIbansCache = new ArrayList<>();
			Object ibanDtls = new Object();
			Map<String, List<String>> map = new HashMap<>();
			String query = "select distinct param_value from ibos_config_param_table where param_key ='ACCOUNT_ID'";
			System.out.println("@@@@@@@@@@@@@@@@@ GET IBAN IDS:"+query);
			try {
				dbUtil = new DBConnectionUtil();
				conn = dbUtil.mGetConnection(url, userName, password);
				pst = conn.prepareStatement(query);
				rs = pst.executeQuery();
				System.out.println(pst);
				while (rs.next()) {
							for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
								ibanDtls = rs.getObject(i);
							}	
							arrIbansCache.add(ibanDtls);							
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (rs != null) {
					rs.close();
				}
				dbUtil.mCloseResources(pst, conn);
				System.out.println(pst);
			}
		}
		System.out.println(arrIbansCache.toString());
		return arrIbansCache;
	}


	public void storeInvalidIbanPain001Dtls(String paymentId, String uniqueGenMsgId, String debtoriBAN, String msgId,
			String pmtInfId, String url, String userName, String password,String status) {
		DBConnectionUtil dbUtil = null;
		PreparedStatement pst = null;
		Connection conn = null;
		String query = "Insert into public.ibos_payment_init (mssg_iden,debtoraccount,orgnl_mssg_iden_name,payment_info_iden,payment_id_ig,transaction_status)values(?,?,?,?,?,?)";
		System.out.println(query);
		try {
			dbUtil = new DBConnectionUtil();
			conn = dbUtil.mGetConnection(url, userName, password);
			pst = conn.prepareStatement(query);
			pst.setString(1, uniqueGenMsgId);
			pst.setString(2, debtoriBAN);
			pst.setString(3, msgId);	
			pst.setString(4, pmtInfId);
			pst.setString(5, paymentId);
			pst.setString(6, status);
			System.out.println(pst);
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			dbUtil.mCloseResources(pst, conn);
			System.out.println(pst);
		}
	}


	public JSONObject getStatusIbanFromDB(String paymentId, String url, String userName, String password) throws SQLException {
		PreparedStatement pst = null;
		DBConnectionUtil dbUtil = null;
		Connection conn = null;
		ResultSet rs = null;
		String paymentStatus = null;
		String debtorIban = null;
		JSONObject statusIbanDtls  = new JSONObject();
		String query = "select transaction_status,debtoraccount from public.ibos_payment_init where payment_id_ig = ?";
		System.out.println(query);
		try {
			dbUtil = new DBConnectionUtil();
			conn = dbUtil.mGetConnection(url, userName, password);
			pst = conn.prepareStatement(query);
			pst.setString(1, paymentId);
			rs = pst.executeQuery();
			System.out.println(pst);
			while (rs.next()) {
				paymentStatus = rs.getString(1);
				debtorIban = rs.getString(2);
				statusIbanDtls.put("status",paymentStatus);
				statusIbanDtls.put("debtorIban",debtorIban);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				rs.close();
			}
			dbUtil.mCloseResources(pst, conn);
			System.out.println(pst);
		}
		return statusIbanDtls;
		
	}


	public String fileNameSequenceGenerator(String url, String userName, String password) throws SQLException {
		PreparedStatement pst = null;
		DBConnectionUtil dbUtil = null;
		Connection conn = null;
		ResultSet rs = null;
		String sequence = "";
		String query = "SELECT lpad(nextval('my_sequence_seq')::text,8,'0')";
		try {
			dbUtil = new DBConnectionUtil();
			conn = dbUtil.mGetConnection(url, userName, password);
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				sequence =  rs.getString(1);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				rs.close();
			}
			dbUtil.mCloseResources(pst, conn);
			System.out.println(pst);
		}
		return sequence;
		
	}


	public void createSeq(String url, String userName, String password) throws SQLException {
		PreparedStatement pst = null;
		DBConnectionUtil dbUtil = null;
		Connection conn = null;
		ResultSet rs = null;
		String sequence="";
		String query = "CREATE SEQUENCE sequence";
		try {
			dbUtil = new DBConnectionUtil();
			conn = dbUtil.mGetConnection(url, userName, password);
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				sequence =  rs.getString(1);
				
			}
	}catch (Exception e) {
		e.printStackTrace();
	} finally {
		if (rs != null) {
			rs.close();
		}
		dbUtil.mCloseResources(pst, conn);
		System.out.println(pst);
	}
	
	}
	
	public String convertToGMTDate(Date date, String format, String timeZone) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);	
		if (timeZone == null || "".equalsIgnoreCase(timeZone.trim())) {
			timeZone = Calendar.getInstance().getTimeZone().getID();
		}		
		sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
		return sdf.format(date);
	}

	
	private void copyMessagetoTomcatServer(String fileName, String content) throws IOException {
		
	    BufferedWriter writer = new BufferedWriter(new FileWriter(localPath+fileName));
	    writer.write(content);
	    writer.close();		    
}
	
	public void updateAHBStatus(String updatedStatus,String paymentId, String url, String userName,
			String password) {
		DBConnectionUtil dbUtil = null;
		PreparedStatement pst = null;
		Connection conn = null;
		boolean output = false;
		String query = "UPDATE public.ibos_payment_init SET ahb_txn_status = ?,count_pain002 = count_pain002+1 where payment_id_ig = ? ";
		System.out.println(query);
		try {
			dbUtil = new DBConnectionUtil();
			conn = dbUtil.mGetConnection(url, userName, password);
			pst = conn.prepareStatement(query);
			pst.setString(1, updatedStatus);
			pst.setString(2, paymentId);	
			pst.execute();
			System.out.println(pst);
			output = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// pst.close();
			dbUtil.mCloseResources(pst, conn);

		}	
	}
	

	public String fetchDate_Seq(String paymentId,String url, String userName,String password) throws SQLException {
		
		PreparedStatement pst = null;
		DBConnectionUtil dbUtil = null;
		Connection conn = null;
		ResultSet rs = null;
		String fetchDate_Seq = null;
		
		String query = "select pain001_date_seq from public.ibos_payment_init where payment_id_ig = ?";
		System.out.println(query);
		try {
			dbUtil = new DBConnectionUtil();
			conn = dbUtil.mGetConnection(url, userName, password);
			pst = conn.prepareStatement(query);
			pst.setString(1, paymentId);
			rs = pst.executeQuery();
			while (rs.next()) {
				fetchDate_Seq = rs.getString(1);
				
				System.out.println(pst);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				rs.close();
			}
			dbUtil.mCloseResources(pst, conn);
			conn.close();
		}
		return fetchDate_Seq;

	}


	
}
