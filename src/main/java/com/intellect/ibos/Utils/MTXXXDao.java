package com.intellect.ibos.Utils;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.intellect.ibos.DBConnectionUtil;
import com.prowidesoftware.swift.model.field.Field61;
import com.prowidesoftware.swift.model.mt.mt9xx.MT940;
import com.prowidesoftware.swift.model.mt.mt9xx.MT942;

public class MTXXXDao {

	
	public boolean persistAccountDetailsFromMT940(MT940 pMT940,Connection conn) throws Exception {
		
		PreparedStatement pst = null;
		DBConnectionUtil dbUtil = null;
		String query = "insert into IBOS_ACCOUNT_BALANCE(BANK_CODE,ACCOUNT_IBAN,IBOS_ACC_ID,IBOS_ACC_DATE,ACC_REFERENCE_NO,STATEMENT_SEQ_NUMBER,OPEN_BAL_MARK,"
				+ "OPEN_BAL_CURRENCY,OPEN_BAL_DATE,OPEN_BAL_AMOUNT,CLOSE_BAL_MARK,CLOSE_BAL_CURRENCY,CLOSE_BAL_DATE,CLOSE_BAL_AMOUNT,CLOSE_AVL_BAL_MARK,"
				+ "CLOSE_AVL_BAL_CURRENCY,CLOSE_AVL_BAL_DATE,CLOSE_AVL_BAL_AMOUNT,FWD_AVL_BAL_MARK,FWD_AVL_BAL_CURRENCY,FWD_AVL_BAL_DATE,FWD_AVL_BAL_AMOUNT,NARRATIVE) "
				+ "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		//nextval('IBOS_ACC_ID_SEQ')
		boolean flag =false; 
		try {
		dbUtil = new DBConnectionUtil();
		pst = conn.prepareStatement(query); 
		
		pst.setString(1, "SAN");
		System.out.println("Tag 25 :"+pMT940.getField25().getValue());
		pst.setString(2, pMT940.getField25().getValue()); //Bank IBAN
		String parent_account_Seq_id = getSequenceNumber("IBOS_ACC_ID_SEQ",conn);
		pst.setString(3, parent_account_Seq_id);
		pst.setObject(4, LocalDate.now()); // local date
		pst.setString(5, pMT940.getField20().getValue()); // reference no
		pst.setString(6, pMT940.getField28C().getValue()); // statement sequence number
		
		pst.setString(7, pMT940.getField60F().getDCMark());
		pst.setString(8, pMT940.getField60F().getCurrency());
		pst.setString(9, pMT940.getField60F().getDate());
		pst.setBigDecimal(10, pMT940.getField60F().amount());
		
		pst.setString(11, pMT940.getField62F().getDCMark());
		pst.setString(12, pMT940.getField62F().getCurrency());
		pst.setString(13,pMT940.getField62F().getDate());
		pst.setBigDecimal(14, pMT940.getField62F().amount());
			
		if( pMT940.getField64()!=null) {
		pst.setString(15, pMT940.getField64().getDCMark());
		pst.setString(16, pMT940.getField64().getCurrency());
		pst.setString(17,pMT940.getField64().getDate());
		pst.setBigDecimal(18, pMT940.getField64().amount());
		
		}else {
			pst.setString(15,"");
			pst.setString(16, "");
			pst.setString(17,"");
			pst.setBigDecimal(18, BigDecimal.ZERO);
		}
	
		if(pMT940.getField65()!=null && pMT940.getField65().size()>=1 ) {
		pst.setString(19, pMT940.getField65().get(0).getDCMark());
		pst.setString(20, pMT940.getField65().get(0).getCurrency());
		pst.setString(21,pMT940.getField65().get(0).getDate());
		pst.setBigDecimal(22, pMT940.getField65().get(0).amount());
		}
		else {
			pst.setString(19,"");
			pst.setString(20, "");
			pst.setString(21,"");
			pst.setBigDecimal(22, BigDecimal.ZERO);
		}
		
		
		
		if(pMT940.getField86()!=null) {
		System.out.println("Last field 86 : "+pMT940.getField86().get(pMT940.getField86().size()-1).getValue());
		pst.setString(23, pMT940.getField86().get(pMT940.getField86().size()-1).getValue());
		}
		else {
			pst.setString(23, null);
		}
		flag = pst.execute();
		System.out.println("Inserted Data successfully");
		flag = true;
		
		if(flag) {
			persistTxnDetailsFromMT940(pMT940, parent_account_Seq_id, conn);
		}
		
		}
		catch(Exception e) {
			
			e.printStackTrace();
			System.out.println(e.getLocalizedMessage());
			throw e;
			
		}
		finally {
			dbUtil.mCloseResources(pst, null);
		}
		return flag;
		
	}
	
	
private String getSequenceNumber(String pSequenceName,Connection conn) {
	PreparedStatement pst = null;
	DBConnectionUtil dbUtil = null;
	String sequencevalue="";
	String query = "select nextval('"+ pSequenceName+"')";
	System.out.println(query);
	try
	{
	dbUtil = new DBConnectionUtil();
	pst = conn.prepareStatement(query); 
	ResultSet rs  = pst.executeQuery();
	if(rs!=null && rs.next()) {
		sequencevalue = rs.getString(1);
		System.out.println(sequencevalue);
	}
	}
	catch(Exception e ) {
		e.printStackTrace();
	}
	finally {
		dbUtil.mCloseResources(pst, null);
	}
	return sequencevalue;
	}


public boolean persistTxnDetailsFromMT940(MT940 pMT940,String parentAccountID,Connection conn) throws Exception {
		
		PreparedStatement pst = null;
		DBConnectionUtil dbUtil = null;
		String query = "insert into IBOS_ACCOUNT_TXN(ACCOUNT_IBAN, IBOS_TXN_ID, PARENT_IBOS_ACC_ID, TXN_REFERENCE_NO,"
				+ " STATEMENT_SEQ_NUMBER, FLOOR_LIMIT_MARK, FLOOR_LIMIT_CURRENCY, FLOOR_LIMIT_AMOUNT,"
				+ " IBOS_TXN_DATE, VALUE_DATE_STRING, ENTRY_DATE_STRING, ACCOUNT_TXN_MARK,"
				+ " FUNDS_CODE, ACCOuNT_TXN_AMOUNT, ACCOUNT_TXN_IDENTIFIER_CODE, REF_ACC_OWNER,"
				+ " REF_ACC_SERV_INSTTUTION, ACC_TXN_SUPP_DTLS, NARRATIVE) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		boolean output =false; 
		try {
		dbUtil = new DBConnectionUtil();
		pst = conn.prepareStatement(query); 
		
		int i =0;
		for(Field61 field61 : pMT940.getField61()) {
	//	System.out.println("Tag 25 :"+pMT940.getField25().getValue());
		pst.setString(1, pMT940.getField25().getValue()); //Bank IBAN
		pst.setObject(2, getSequenceNumber("IBOS_TXN_ID_SEQ", conn)); 
		pst.setString(3, parentAccountID); // reference no
		pst.setString(4, pMT940.getField20().getValue()); // reference no
		pst.setString(5, pMT940.getField28C().getValue()); // statement sequence number
		//Floor Limit not there for 940
		pst.setString(6, "");
		pst.setString(7, "");
		pst.setBigDecimal(8, BigDecimal.ZERO);
		pst.setObject(9, LocalDateTime.now());
		pst.setString(10, field61.getValueDate());
		pst.setString(11, field61.getEntryDate());
		
		pst.setString(12,field61.getDCMark());
		pst.setString(13, field61.getFundsCode());
			
		pst.setBigDecimal(14, field61.amount());
		pst.setString(15, field61.getIdentificationCode());
		pst.setString(16,field61.getReferenceForTheAccountOwner());
		pst.setString(17, field61.getReferenceOfTheAccountServicingInstitution());
		pst.setString(18,field61.getSupplementaryDetails());
		pst.setString(19, pMT940.getField86().get(i).getNarrative());
		pst.addBatch();
		i++;
		}
	
		 pst.executeBatch();
		System.out.println("Inserted Data successfully");
		output= true;
		}
		catch(Exception e) {
			
			e.printStackTrace();
			System.out.println(e.getLocalizedMessage());
			output= false;
			throw e;
			
		}
		finally {
			dbUtil.mCloseResources(pst, null);
		}
		return output;
		
	}
	
	
public boolean persistTxnDetailsFromMT942(MT942 pMT942,String parentAccountID,Connection conn) throws Exception {
	
	PreparedStatement pst = null;
	DBConnectionUtil dbUtil = null;
	String query = "insert into IBOS_ACCOUNT_TXN(ACCOUNT_IBAN, IBOS_TXN_ID, PARENT_IBOS_ACC_ID, TXN_REFERENCE_NO,"
			+ " STATEMENT_SEQ_NUMBER, FLOOR_LIMIT_MARK, FLOOR_LIMIT_CURRENCY, FLOOR_LIMIT_AMOUNT,"
			+ " IBOS_TXN_DATE, VALUE_DATE_STRING, ENTRY_DATE_STRING, ACCOUNT_TXN_MARK,"
			+ " FUNDS_CODE, ACCOuNT_TXN_AMOUNT, ACCOUNT_TXN_IDENTIFIER_CODE, REF_ACC_OWNER,"
			+ " REF_ACC_SERV_INSTTUTION, ACC_TXN_SUPP_DTLS, NARRATIVE) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	boolean output =false; 
	try {
	dbUtil = new DBConnectionUtil();
	pst = conn.prepareStatement(query); 
	
	int i =0;
	for(Field61 field61 : pMT942.getField61()) {
//	System.out.println("Tag 25 :"+pMT940.getField25().getValue());
	pst.setString(1, pMT942.getField25().getValue()); //Bank IBAN
	pst.setObject(2, getSequenceNumber("IBOS_TXN_ID_SEQ", conn)); 
	pst.setString(3, parentAccountID); // reference no
	pst.setString(4, pMT942.getField20().getValue()); // reference no
	pst.setString(5, pMT942.getField28C().getValue()); // statement sequence number
	pst.setString(6, pMT942.getField34F().get(0).getDCMark());
	pst.setString(7, pMT942.getField34F().get(0).getCurrency());
	pst.setBigDecimal(8, pMT942.getField34F().get(0).amount());
	pst.setObject(9, LocalDateTime.now());
	pst.setString(10, field61.getValueDate());
	pst.setString(11, field61.getEntryDate());
	
	pst.setString(12,field61.getDCMark());
	pst.setString(13, field61.getFundsCode());
		
	pst.setBigDecimal(14, field61.amount());
	pst.setString(15, field61.getIdentificationCode());
	pst.setString(16,field61.getReferenceForTheAccountOwner());
	pst.setString(17, field61.getReferenceOfTheAccountServicingInstitution());
	pst.setString(18,field61.getSupplementaryDetails());
	if (pMT942.getField86()!=null && pMT942.getField86().size()>=1) {
	pst.setString(19, pMT942.getField86().get(i).getNarrative());
	}
	else {
		pst.setString(19, "");
	}
	pst.addBatch();
	i++;
	}

	 pst.executeBatch();
	System.out.println("Inserted Data successfully");
	output= true;
	}
	catch(Exception e) {
		
		e.printStackTrace();
		System.out.println(e.getLocalizedMessage());
		output= false;
		throw e;
		
	}
	finally {
		dbUtil.mCloseResources(pst, null);
	}
	return output;
	
}

public Map<String, Object> fetchAccountsDetailsFromDB(String accountDate,String accountID,Connection conn) throws Exception {
	
	PreparedStatement pst = null;
	DBConnectionUtil dbUtil = null;
	System.out.println(" accountDate : "+ accountDate + " accountID "+accountID);
	String query = "SELECT * FROM ibos_account_balance where account_iban = ?";
	//String query = "SELECT * FROM ibos_account_balance";
	//SELECT * FROM ibos_account_balance where account_iban = '032000000010' and ibos_acc_date = to_date ('2020-11-29','yyyy-MM-dd'); 
	//nextval('IBOS_ACC_ID_SEQ')
	ResultSet rs =null; 
	Map<String, Object> map = new HashMap<String,Object>();
	try {
	dbUtil = new DBConnectionUtil();
	System.out.println(" query "+query);;
	pst = conn.prepareStatement(query); 
	pst.setString(1, accountID);
//	pst.setString(2, accountDate);
	rs = pst.executeQuery();
	System.out.println("Fetched Data successfully");
	
	while(rs.next()) {
		Date ibos_acc_date = rs.getDate("ibos_acc_date");
		Date ibosDate = new Date(ibos_acc_date.getTime());
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		System.out.println("inside result set");
		for(int  i=1;i<= rs.getMetaData().getColumnCount();i++)
			//filter 
			if(sf.format(ibosDate).equalsIgnoreCase(sf.format(new java.util.Date()))) {
			map.put(rs.getMetaData().getColumnLabel(i).toUpperCase(),rs.getObject(i));
			}
			else {
				System.out.println(" not found !!! account info");
			}
	}
	
	System.out.println(" data "+map);
	}
	catch(Exception e) {
		
		e.printStackTrace();
		System.out.println(e.getLocalizedMessage());
		throw e;
		
	}
	finally {
		rs.close();
		dbUtil.mCloseResources(pst, null);
	}
	return map;
	
}


public List<Map<String, Object>> fetchTransactionsDetailsFromDB(String accountToDate, String accountFromDate,String accountID,Connection conn) throws Exception {
	
	PreparedStatement pst = null;
	DBConnectionUtil dbUtil = null;
//	String query = "SELECT * FROM ibos_account_txn where account_iban = ?";
	String query = "SELECT * FROM ibos_account_txn where account_iban = ?";
	//SELECT * FROM ibos_account_balance where account_iban = '032000000010' and ibos_acc_date = to_date ('2020-11-29','yyyy-MM-dd'); 
	//nextval('IBOS_ACC_ID_SEQ')
	System.out.println(accountID);
	System.out.println(accountFromDate);
	System.out.println(accountToDate);

	ResultSet rs =null; 
	List<Map<String, Object>> datalist = new ArrayList();
	try {
	dbUtil = new DBConnectionUtil();
	pst = conn.prepareStatement(query); 
	pst.setString(1, accountID);
//	pst.setString(2, accountFromDate);
//	pst.setString(3, accountToDate);
	rs = pst.executeQuery();
	System.out.println("Fetched Data successfully");
	
	while(rs.next()) {
		Map<String,Object> tempMap = new HashMap<String,Object>();
		
		try {
			//Date ibos_TXN_date = rs.getDate("ibos_TXN_date");
			String ibosValueDateString = rs.getString(IBOSConstants.VALUE_DATE_STRING);
			SimpleDateFormat sf2 = new SimpleDateFormat("yyMMdd");
			java.util.Date ibosDate = new java.util.Date();
			try {
				ibosDate =sf2.parse(ibosValueDateString);
				System.out.println(" Parsed Date valueDate String : "+ibosDate);
			}
			catch(Exception e) {
				ibosDate = new java.util.Date();
			}
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date fromDate = sf.parse(accountFromDate);
			java.util.Date toDate = sf.parse(accountToDate);

			System.out.println(fromDate +"   "+toDate+"   "+ibosDate);
			
			if ((ibosDate.after(fromDate) && ibosDate.before(toDate)) || (
					sf.format(ibosDate).equalsIgnoreCase(sf.format(fromDate))) || (
					sf.format(ibosDate).equalsIgnoreCase(sf.format(toDate)))) {
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {

					tempMap.put(rs.getMetaData().getColumnLabel(i).toUpperCase(), rs.getObject(i));

				}
				
				datalist.add(tempMap);
				System.out.println(" tepm Map"+tempMap);
			}

		} catch (Exception e) {
			System.out.println(" exception :" + e.getMessage());
		}

		

	}
	
	System.out.println(" data "+datalist);
	}
	catch(Exception e) {
		
		e.printStackTrace();
		System.out.println(e.getLocalizedMessage());
		throw e;
		
	}
	finally {
		rs.close();
		dbUtil.mCloseResources(pst, null);
	}
	return datalist;
	
}


}
