package com.intellect.ibos.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URL;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.ClientFilter;

public class ISPauthorization {
	
	private static ClientConfig config = null;
	private static Client client = null;
	public Client authenticatedClient;
	public final static String AUTHORIZATION_HEADER = "Authorization";	
	private static final Pattern pat = Pattern.compile(".*\"access_token\"\\s*:\\s*\"([^\"]+)\".*");
	private static final String clientId = "36cfcec4-4394-47de-80e3-a6cef8a1fdbf";//clientId
	private static final String clientSecret = "8799da77-0a0c-4d10-9707-81e5115e9e5c";//client secret
	private static final String tokenUrl = "https://sdarwin-api-external.intesasanpaolo.com/auth/oauth/v2/token";
	private static final String auth = clientId + ":" + clientSecret;
	private static final String authentication = Base64.getEncoder().encodeToString(auth.getBytes());
	
	static {
		config = new DefaultClientConfig();
		client = Client.create(config);
	}

	public ClientResponse performPOST(String url, String accessToken, String reqJson) {
		System.out.println("\n performPOST=====> \n url:"+url+"\n ------reqJson------ \n"+reqJson);
		if (authenticatedClient == null) {
			authenticatedClient = Client.create();
			authenticatedClient.addFilter(new OAuth2RequestFilter(accessToken));
		}
		WebResource webResource = authenticatedClient.resource(url);
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, reqJson);

		if(!response.getClientResponseStatus().equals(ClientResponse.Status.OK)){
			//accessToken.setAccessToken(null);		
			authenticatedClient.removeAllFilters();
		}
		System.out.println("\n ------performPost------ \n"+response.toString());
		return response;
	}

	public ClientResponse performGet(String url, String accessToken) throws Exception {
		System.out.println("\n ------performGet------ \n"+url);
		if (authenticatedClient == null) {
			authenticatedClient = Client.create();
			authenticatedClient.addFilter(new OAuth2RequestFilter(accessToken));
		}
		WebResource webResource = authenticatedClient.resource(url);

		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
		if(!response.getClientResponseStatus().equals(ClientResponse.Status.OK)){
			//accessToken.setAccessToken(null);		
			authenticatedClient.removeAllFilters();
		}
		return response;
	}

	private class OAuth2RequestFilter extends ClientFilter {
		private String oauth2TokenHeader;
		public OAuth2RequestFilter(String oauth2Token) {
			this.oauth2TokenHeader = "Bearer " + oauth2Token;
		}
		@Override
		public ClientResponse handle(ClientRequest request)
				throws ClientHandlerException {
			request.getProperties().put(AUTHORIZATION_HEADER, oauth2TokenHeader);
			request.getHeaders().putSingle(AUTHORIZATION_HEADER, oauth2TokenHeader);
			return getNext().handle(request);
		}		
	}
	
	public String getClientCredentials() throws Exception {
	    String content = "grant_type=client_credentials";
	    BufferedReader reader = null;
	    HttpsURLConnection connection = null;
	    String returnValue = "";
	    try {
	        URL url = new URL(tokenUrl);
	        connection = (HttpsURLConnection) url.openConnection();
	        connection.setRequestMethod("POST");
	        connection.setDoOutput(true);
	        connection.setRequestProperty("Authorization", "Basic " + authentication);
	        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	        connection.setRequestProperty("Accept", "application/json");
	        PrintStream os = new PrintStream(connection.getOutputStream());
	        os.print(content);
	        os.close();
	        reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	        String line = null;
	        StringWriter out = new StringWriter(connection.getContentLength() > 0 ? connection.getContentLength() : 2048);
	        while ((line = reader.readLine()) != null) {
	            out.append(line);
	        }
	        String response = out.toString();
	        Matcher matcher = pat.matcher(response);
	        if (matcher.matches() && matcher.groupCount() > 0) {
	            returnValue = matcher.group(1);
	        }
	        System.out.println("getClientCredentials response : " + response);
	    } catch (Exception e) {
	        System.out.println("Error : " + e.getMessage());
	        throw new Exception("Access Token generation error.");
	    } finally {
	        if (reader != null) {
	            try {
	                reader.close();
	            } catch (IOException e) {
	            }
	        }
	        connection.disconnect();
	    }
	    return returnValue;
	}
	
	static void fetchtest(String bearerToken, String resource, String method) {
	    BufferedReader reader = null;
	    try {
	    	URL url = new URL(resource);
	        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
	        connection.setRequestProperty("Authorization", "Bearer " + bearerToken);
	        connection.setDoOutput(true);
	        connection.setRequestMethod(method); // "GET"
	        reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	        String line = null;
	        StringWriter out = new StringWriter(connection.getContentLength() > 0 ? connection.getContentLength() : 2048);
	        while ((line = reader.readLine()) != null) {
	            out.append(line);
	        }
	        String response = out.toString();
	        System.out.println("Accounts Response:"+response);
	    } catch (Exception e) {
	    
	    }
	}
		
	/*
	 * public static void main(String[] args) throws Exception {
	 * 
	 * ISPauthorization o = new ISPauthorization(); WebResource resource = null;
	 * String bearerToken=o.getClientCredentials(); resource =
	 * client.resource(getBaseURI("https://sdarwin-api-external.intesasanpaolo.com")
	 * ). path(
	 * "/sandbox/v1/poc/payments/sepa-credit-transfers/6653b58b-0726-4659-92f7-0db650f62821/status"
	 * ); String
	 * req="{ \"instructedAmount\":{ \"currency\":\"EUR\", \"amount\":12.00 }," +
	 * " \"debtorAccount\":{ \"iban\":\"IT03Y0306909473100000003718\" }, " +
	 * "\"creditor\":{ \"name\":\"Creditore tedesco\" }, \"creditorAccount\":{ \"iban\":\"DE231001200201234567894\" }, "
	 * + "\"endToEndIdentification\":\"qualsiasi valore not null\", " +
	 * "\"remittanceInformationUnstructured\":\"qualsiasi valore\", " +
	 * "\"creditorAgent\":\"qualsiasi valore\", \"creditorAddress\":\"qualsiasi valore\" }"
	 * .replaceAll("\\r", ""); o.performGet(resource.toString(),bearerToken);
	 * //ISPauthorization.fetchtest(bearerToken, resource.toString(), "GET"); }
	 */

	/*
	 * private static URI getBaseURI(String baseURI) { return
	 * UriBuilder.fromUri(baseURI).build(); } private String
	 * getResponse(ClientResponse response) throws Exception {
	 * 
	 * return response.getEntity(String.class); }
	 */
}

