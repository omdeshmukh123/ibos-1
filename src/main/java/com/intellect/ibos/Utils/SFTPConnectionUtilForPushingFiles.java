package com.intellect.ibos.Utils;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class SFTPConnectionUtilForPushingFiles {

	private static final Logger logger = LogManager.getLogger(SFTPConnectionUtilForPushingFiles.class);
	ChannelSftp channelSftp = null;
	Session session = null;
//
//	@Value("${SAN_SFTP_USER_ID}")
//	String sanSftpUserId;
//
//	@Value("${SAN_SFTP_HOST}")
//	String sanHostId;
//
//	@Value("${SAN_SFTP_PORT}")
//	int sanSftpPort;
//
//	@Value("${SAN_SFTP_PASSWORD}")
//	String sanSftpPassword;

	public ChannelSftp getSFTPConnectionforPushingFiles() throws IOException {

		try {
			JSch jsch = new JSch();
			session = jsch.getSession(IBOSConstants.sanSftpUserId, IBOSConstants.sanHostId, 6741);
			session.setPassword(IBOSConstants.sftpPassword);
			System.out.println("SFTP User Id For Pushing Outgoing File to Santander:" + IBOSConstants.sanSftpUserId);
			System.out.println("SFTP hostId For Pushing Outgoing File to Santander:" + IBOSConstants.sanHostId);
			System.out.println("SFTP Port For Pushing Outgoing File to Santander:" + 6741);
			System.out.println("SFTP Password For Pushing Outgoing File to Santander:" + IBOSConstants.sanSftpPassword);
			session.setPassword(IBOSConstants.sanSftpPassword);		
			java.util.Properties config = new java.util.Properties();
			config.put(IBOSConstants.STRICT_HOST_KEY_CHECK, IBOSConstants.NO);
			System.out.println("Disabled STRICT HOST KEY CHECK For Pushing Outgoing File to Santander:" + IBOSConstants.STRICT_HOST_KEY_CHECK +IBOSConstants.NO);
			session.setConfig(config);
			session.connect(10000);
			channelSftp = (ChannelSftp) session.openChannel(IBOSConstants.SFTP);
			channelSftp.connect();
			logger.info("Pushing Connection Obtained :" + channelSftp);
		} catch (Exception ex) {
			logger.error("Unable to get connection for Pushing", ex);
		}
		return channelSftp;
	}

	public void closeConnection() {
		logger.info("Pushing Connection closed :");
		channelSftp.exit();
		if (channelSftp.isConnected()) {
			channelSftp.disconnect();
			if (logger.isInfoEnabled()) {
				logger.info("Channel Sftp Disconnected");
			}
		}
		if (session != null && session.isConnected()) {
			session.disconnect();
			if (logger.isInfoEnabled()) {
				logger.info("Session Disconnected");
			}
		}
	}
}
