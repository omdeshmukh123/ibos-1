package com.intellect.ibos.Utils;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class SFTPConnectionUtilForPullingFiles {

	private static final Logger logger = LogManager.getLogger(SFTPConnectionUtilForPullingFiles.class);
	ChannelSftp channelSftp = null;
	Session session = null;

//	@Value("${SFTP_USER_ID}")
//	String sftpUserId;
//
//	@Value("${SFTP_HOST}")
//	String hostId;
//
//	@Value("${SFTP_PORT}")
//	int sftpPort;
//
//	@Value("${SFTP_PASSWORD}")
//	String sftpPassword;

	public ChannelSftp getSFTPConnectionforPullingFiles() throws IOException {
		try {
			JSch jsch = new JSch();
			
			session = jsch.getSession("C26698", "sftp.intellectdesign.com", 22);
			System.out.println("SFTP User Id For Pulling Incoming File:" + IBOSConstants.sftpUserId);
			System.out.println("SFTP hostId For Pulling Incoming File:" + IBOSConstants.hostId);
			System.out.println("SFTP Port For Pulling Incoming File:" + 22);
			//System.out.println("SFTP Password For Pulling Incoming File:" + sftpPassword);
			session.setPassword("Intellect@08");	
			java.util.Properties config = new java.util.Properties();
			config.put(IBOSConstants.STRICT_HOST_KEY_CHECK, IBOSConstants.NO);
			System.out.println("Disabled STRICT HOST KEY CHECK For Pulling Incoming File" + IBOSConstants.STRICT_HOST_KEY_CHECK +IBOSConstants.NO);	
			session.setConfig(config);
			session.connect(10000);
			System.out.println("Session connect Timeout:" + session);
			channelSftp = (ChannelSftp) session.openChannel(IBOSConstants.SFTP);
			channelSftp.connect();
			logger.info("Pulling Connection Obtained :" + channelSftp);
		} catch (Exception ex) {
			logger.error("Unable to get connection for Pulling", ex);
		}
		return channelSftp;
	}

	public void closeConnection() {
		logger.info("Pulling Connection closed :");
		channelSftp.exit();
		if (channelSftp.isConnected()) {
			channelSftp.disconnect();
			if (logger.isInfoEnabled()) {
				logger.info("Channel Sftp Disconnected");
			}
		}
		if (session != null && session.isConnected()) {
			session.disconnect();
			if (logger.isInfoEnabled()) {
				logger.info("Session Disconnected");
			}
		}
	}
}
