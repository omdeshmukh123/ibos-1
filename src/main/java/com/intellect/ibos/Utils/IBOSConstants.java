package com.intellect.ibos.Utils;

public class IBOSConstants {

	public static final String amount = "amount";
	public static final String account = "account";
	public static String ACCOUNT_IBAN = "ACCOUNT_IBAN";
	public static String TXN_REFERENCE_NO = "TXN_REFERENCE_NO";
	public static String STATEMENT_SEQ_NUMBER = "STATEMENT_SEQ_NUMBER";
	public static String FLOOR_LIMIT_MARK = "FLOOR_LIMIT_MARK";
	public static String FLOOR_LIMIT_CURRENCY = "FLOOR_LIMIT_CURRENCY";
	public static String FLOOR_LIMIT_AMOUNT = "FLOOR_LIMIT_AMOUNT";
	public static String IBOS_TXN_DATE = "IBOS_TXN_DATE";
	public static String VALUE_DATE_STRING = "VALUE_DATE_STRING";
	public static String ENTRY_DATE_STRING = "ENTRY_DATE_STRING";
	public static String ACCOUNT_TXN_MARK = "ACCOUNT_TXN_MARK";
	public static String FUNDS_CODE = "FUNDS_CODE";
	public static String ACCOuNT_TXN_AMOUNT = "ACCOUNT_TXN_AMOUNT";
	public static String ACCOUNT_TXN_IDENTIFIER_CODE = "ACCOUNT_TXN_IDENTIFIER_COD";
	public static String REF_ACC_OWNER = "REF_ACC_OWNER";
	public static String REF_ACC_SERV_INSTTUTION = "REF_ACC_SERV_INSTTUTION";
	public static String ACC_TXN_SUPP_DTLS = "ACC_TXN_SUPP_DTLS";
	public static String NARRATIVE = "NARRATIVE";
	public static String BANK_CODE = "BANK_CODE";
	public static String ACC_REFERENCE_NO = "ACC_REFERENCE_NO";
	public static String OPEN_BAL_MARK = "OPEN_BAL_MARK";
	public static String OPEN_BAL_CURRENCY = "OPEN_BAL_CURRENCY";
	public static String OPEN_BAL_DATE = "OPEN_BAL_DATE";
	public static String OPEN_BAL_AMOUNT = "OPEN_BAL_AMOUNT";
	public static String CLOSE_BAL_MARK = "CLOSE_BAL_MARK";
	public static String CLOSE_BAL_CURRENCY = "CLOSE_BAL_CURRENCY";
	public static String CLOSE_BAL_DATE = "CLOSE_BAL_DATE";
	public static String CLOSE_BAL_AMOUNT = "CLOSE_BAL_AMOUNT";
	public static String CLOSE_AVL_BAL_MARK = "CLOSE_AVL_BAL_MARK";
	public static String CLOSE_AVL_BAL_CURRENCY = "CLOSE_AVL_BAL_CURRENCY";
	public static String CLOSE_AVL_BAL_DATE = "CLOSE_AVL_BAL_DATE";
	public static String CLOSE_AVL_BAL_AMOUNT = "CLOSE_AVL_BAL_AMOUNT";
	public static String FWD_AVL_BAL_MARK = "FWD_AVL_BAL_MARK";
	public static String FWD_AVL_BAL_CURRENCY = "FWD_AVL_BAL_CURRENCY";
	public static String FWD_AVL_BAL_DATE = "FWD_AVL_BAL_DATE";
	public static String FWD_AVL_BAL_AMOUNT = "FWD_AVL_BAL_AMOUNT";
	
	public static String PARAM_KEY = "PARAM_KEY";
	public static String PARAM_VALUE = "PARAM_VALUE";
	public static String CATEGORY = "CATEGORY";
	public static String SUB_CATEGORY = "SUB_CATEGORY";
	
	
	
	public static String NORDEA_BANK = "NOR";
	public static String SANTANDER_BANK = "SAN";
	public static String ISP_BANK = "ISP";
	public static String PBZ_BANK = "PBZ";
	
	public static String FI = "FI";
	public static String DE = "DE";
	public static String HR = "HR";
	public static String IT = "IT";
	
	
	//JSON Fields
	public static String iban ="iban";
	public static String currency ="currency";
	public static String ownerName ="ownerName";
	public static String name ="name";
	public static String balances ="balances";
	public static String cashAccountType ="cashAccountType";
	public static String status ="status";
	public static String account_short_name ="account_short_name";
	public static String account_name ="account_name";
	public static String balanceAmount ="balanceAmount";
	public static String balanceType ="balanceType";
	public static String referenceDate ="referenceDate";
	public static String lastChangedDateTime ="lastChangedDateTime";
	public static String lastCommittedTransaction ="lastCommittedTransaction";
	public static String Amount ="Amount";
	public static String BalanceType ="Balance Type";
	public static String ISODate ="ISODate";
	public static String ISODateTime ="ISODateTime";
	public static String Max35Text ="Max35Text";
	
	public static String NORDEA = "NOR";
	public static String HB_SANTANDER = "BSCHES";
	public static String HB_ISP = "BCITIT";
	public static String HB_PBZ = "PBZGHR";
	public static final String RECEIVER_KEYSTORE = "/usr1/SIR21466/Store/MyKeys/publickey.cer";
	//public static final String RECEIVER_KEYSTORE = "MyKeys/publickey.cer";
	public static final String SFTP = "sftp";
	public static final Object STRICT_HOST_KEY_CHECK = "StrictHostKeyChecking";
	public static final Object NO = "no";
	
	public static final String sftpUserId="C26698";
	public static final String hostId ="sftp.intellectdesign.com";
	public static final String sftpPassword = "Intellect@08";
	
	public static final String sanSftpUserId="sftpibintdepre";
	public static final String sanHostId ="193.127.189.242";
	public static final String sanSftpPassword = "sftpibintdepre01";

}
