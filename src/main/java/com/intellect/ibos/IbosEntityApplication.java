package com.intellect.ibos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = "com.intellect.ibos*")
@EnableScheduling
public class IbosEntityApplication extends SpringBootServletInitializer  {

	public static void main(String[] args) {
		SpringApplication.run(IbosEntityApplication.class, args);
	}

}
